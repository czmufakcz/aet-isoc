package com.aetisoc.services;

import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aetisoc.email.EmailService;
import com.aetisoc.security.domain.User;
import com.aetisoc.utils.BaseIntegrationTest;

public class EmailTriggerTest extends BaseIntegrationTest {

    private static final User MAIL_TARGET = new User(UUID.randomUUID(), "Martin", "Lepeška", "nope", "martin.lepeska@gmail.com", "711 111 222", 
    		false, true, LocalDateTime.MAX, LocalDateTime.MAX);

    @Autowired
    EmailService mailService;
    
    @Ignore("We don't want to get our account marked as SPAMMER")
    @Test
    public void sendOffersCreatedEmailTrigger() throws Exception {
    	mailService.sendOffersCreatedEmail(MAIL_TARGET);
    }
    
    @Ignore("We don't want to get our account marked as SPAMMER")
    @Test
    public void sendNoOffersEmailTrigger() throws Exception {
    	mailService.sendNoOffersEmail(MAIL_TARGET);
    }
	
}
