package com.aetisoc.pathgenerator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.aetisoc.offer.domain.Connection;
import com.aetisoc.offer.model.ConnectionService;
import com.aetisoc.offer.model.OfferService;
import com.aetisoc.path.generator.Path;
import com.aetisoc.path.generator.PathService;
import com.aetisoc.utils.BaseIntegrationTest;

public class PathGeneratorTest extends BaseIntegrationTest {

    @MockBean
    ConnectionService connectionService;

    @Autowired
    PathService pathService;

    @MockBean
    OfferService offerService;

    private final Connection criticalConnection = Connection.builder()
                                                            .fromCity("x")
                                                            .toCity("y")
                                                            .bandwidth(1)
                                                            .duration(5)
                                                            .build();

    @Test
    public void findBestPath() throws Exception {
        // Given
        givenMap();
        givenNoReservations();

        List<Path> paths = pathService.generatePaths("x", "z", 1, LocalDateTime.now(), LocalDateTime.MAX);

        System.out.println("Found " + paths.size() + " variants!");
        for (Path path : paths) {
            System.out.println(path);
        }

        assertEquals(12, paths.get(0)
                              .getTimeLength());
    }

    @Test
    public void findBestAlternativeWhenBestPathIsBlocked() throws Exception {
        // Given
        givenMap();
        givenNoReservations();
        given(offerService.isConnectionAvailable(eq(criticalConnection), any(), any(), anyInt())).willReturn(false);

        List<Path> paths = pathService.generatePaths("x", "z", 1, LocalDateTime.now(), LocalDateTime.MAX);

        System.out.println("Found " + paths.size() + " variants!");
        for (Path path : paths) {
            System.out.println(path);
        }

        assertEquals(12, paths.get(0)
                              .getTimeLength());

        for (Path path : paths) {
            if (path.getConnections()
                    .contains(criticalConnection)) {
                fail();
            }
        }
    }

    private void givenMap() {
        given(connectionService.findAllByFromCity("x")).willReturn(Arrays.asList(criticalConnection, Connection.builder()
                                                                                                               .fromCity("x")
                                                                                                               .toCity("w")
                                                                                                               .bandwidth(1)
                                                                                                               .duration(7)
                                                                                                               .build(), Connection.builder()
                                                                                                                                   .fromCity("x")
                                                                                                                                   .toCity("v")
                                                                                                                                   .bandwidth(1)
                                                                                                                                   .duration(10)
                                                                                                                                   .build()));

        given(connectionService.findAllByFromCity("y")).willReturn(Arrays.asList(Connection.builder()
                                                                                           .fromCity("y")
                                                                                           .toCity("x")
                                                                                           .bandwidth(1)
                                                                                           .duration(5)
                                                                                           .build(), Connection.builder()
                                                                                                               .fromCity("y")
                                                                                                               .toCity("z")
                                                                                                               .bandwidth(1)
                                                                                                               .duration(7)
                                                                                                               .build()));

        given(connectionService.findAllByFromCity("z")).willReturn(Arrays.asList(Connection.builder()
                                                                                           .fromCity("z")
                                                                                           .toCity("y")
                                                                                           .bandwidth(1)
                                                                                           .duration(7)
                                                                                           .build(), Connection.builder()
                                                                                                               .fromCity("z")
                                                                                                               .toCity("w")
                                                                                                               .bandwidth(1)
                                                                                                               .duration(5)
                                                                                                               .build(), Connection.builder()
                                                                                                                                   .fromCity("z")
                                                                                                                                   .toCity("t")
                                                                                                                                   .bandwidth(1)
                                                                                                                                   .duration(5)
                                                                                                                                   .build(), Connection.builder()
                                                                                                                                                       .fromCity("z")
                                                                                                                                                       .toCity("u")
                                                                                                                                                       .bandwidth(1)
                                                                                                                                                       .duration(4)
                                                                                                                                                       .build()));

        given(connectionService.findAllByFromCity("w")).willReturn(Arrays.asList(Connection.builder()
                                                                                           .fromCity("w")
                                                                                           .toCity("x")
                                                                                           .bandwidth(1)
                                                                                           .duration(7)
                                                                                           .build(), Connection.builder()
                                                                                                               .fromCity("w")
                                                                                                               .toCity("z")
                                                                                                               .bandwidth(1)
                                                                                                               .duration(5)
                                                                                                               .build(), Connection.builder()
                                                                                                                                   .fromCity("w")
                                                                                                                                   .toCity("v")
                                                                                                                                   .bandwidth(1)
                                                                                                                                   .duration(8)
                                                                                                                                   .build()));

        given(connectionService.findAllByFromCity("v")).willReturn(Arrays.asList(Connection.builder()
                                                                                           .fromCity("v")
                                                                                           .toCity("x")
                                                                                           .bandwidth(1)
                                                                                           .duration(10)
                                                                                           .build(), Connection.builder()
                                                                                                               .fromCity("v")
                                                                                                               .toCity("w")
                                                                                                               .bandwidth(1)
                                                                                                               .duration(8)
                                                                                                               .build(), Connection.builder()
                                                                                                                                   .fromCity("v")
                                                                                                                                   .toCity("u")
                                                                                                                                   .bandwidth(1)
                                                                                                                                   .duration(4)
                                                                                                                                   .build()));

        given(connectionService.findAllByFromCity("u")).willReturn(Arrays.asList(Connection.builder()
                                                                                           .fromCity("u")
                                                                                           .toCity("z")
                                                                                           .bandwidth(1)
                                                                                           .duration(8)
                                                                                           .build(), Connection.builder()
                                                                                                               .fromCity("u")
                                                                                                               .toCity("s")
                                                                                                               .bandwidth(1)
                                                                                                               .duration(2)
                                                                                                               .build(), Connection.builder()
                                                                                                                                   .fromCity("u")
                                                                                                                                   .toCity("v")
                                                                                                                                   .bandwidth(1)
                                                                                                                                   .duration(4)
                                                                                                                                   .build()));

        given(connectionService.findAllByFromCity("s")).willReturn(Arrays.asList(Connection.builder()
                                                                                           .fromCity("s")
                                                                                           .toCity("u")
                                                                                           .bandwidth(1)
                                                                                           .duration(2)
                                                                                           .build(), Connection.builder()
                                                                                                               .fromCity("s")
                                                                                                               .toCity("t")
                                                                                                               .bandwidth(1)
                                                                                                               .duration(2)
                                                                                                               .build()));

        given(connectionService.findAllByFromCity("t")).willReturn(Arrays.asList(Connection.builder()
                                                                                           .fromCity("t")
                                                                                           .toCity("s")
                                                                                           .bandwidth(1)
                                                                                           .duration(2)
                                                                                           .build(), Connection.builder()
                                                                                                               .fromCity("t")
                                                                                                               .toCity("z")
                                                                                                               .bandwidth(1)
                                                                                                               .duration(5)
                                                                                                               .build()));
    }

    private void givenNoReservations() {
        given(offerService.isConnectionAvailable(any(), any(), any(), anyInt())).willReturn(true);
    }

}
