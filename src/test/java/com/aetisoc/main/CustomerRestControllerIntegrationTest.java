package com.aetisoc.main;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.aetisoc.security.domain.Businessman;
import com.aetisoc.security.domain.Customer;
import com.aetisoc.security.domain.ROLE;
import com.aetisoc.security.model.Address;
import com.aetisoc.security.model.CustomerRepository;
import com.aetisoc.utils.BaseIntegrationTest;

public class CustomerRestControllerIntegrationTest extends BaseIntegrationTest {

    private final String URL_API = "/api/customers";

    private final String id = "2757b7bf-e149-4827-b615-3cc65c4e3d73";
    private final String firstname = "Bohumil";
    private final String lastname = "Novák";
    private final String email = "bohumil@example.com";
    private final String phone = "739444555";
    private final String password = "password";
    private final String role = "CUSTOMER";
    private final String cin = "65454666";
    private final String vatin = "564644688888";
    private final Address address = new Address(new Long(1), "Brnenskeho", "22", "Brno", "533 22");
    
    private Customer createdCustomer = Customer.builder()
									           .id(UUID.fromString(id))
									           .firstname(firstname)
									           .lastname(lastname)
									           .email(email)
									           .phone(phone)
									           .password(password)
									           .company(false)
                                               .address(address)
									           .build();
    
    private Customer createdCompany = Customer.builder()
									       .id(UUID.fromString(id))
									       .firstname(firstname)
									       .lastname(lastname)
									       .email(email)
									       .phone(phone)
									       .password(password)
									       .company(true)
									       .cin(cin)
									       .vatin(vatin)
									       .address(address)
									       .build();
								    
    @MockBean
    private CustomerRepository customerRepository;

    @Test
    public void createValidCompanyTest() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCompany);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Customer.builder()
                                                                     .firstname(firstname)
                                                                     .lastname(lastname)
                                                                     .email(email)
                                                                     .phone(phone)
                                                                     .password(password)
                                                                     .company(true)
                                                                     .cin(cin)
                                                                     .vatin(vatin)
                                                                     .address(address)
                                                                     .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id", is(id)))
               .andExpect(jsonPath("$.firstname", is(firstname)))
               .andExpect(jsonPath("$.lastname", is(lastname)))
               .andExpect(jsonPath("$.email", is(email)))
               .andExpect(jsonPath("$.phone", is(phone)))
               .andExpect(jsonPath("$.role", is(role)))
               .andExpect(jsonPath("$.company", is(true)))
               .andExpect(jsonPath("$.cin", is(cin)))
               .andExpect(jsonPath("$.vatin", is(vatin)))
               .andExpect(jsonPath("$.address.street", is(address.getStreet())))
               .andExpect(jsonPath("$.address.descriptionNumber", is(address.getDescriptionNumber())))
               .andExpect(jsonPath("$.address.city", is(address.getCity())))
               .andExpect(jsonPath("$.address.postcode", is(address.getPostcode())));
    }
    
    @Test
    public void doNotCreateCompanyWithoutCin() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCompany);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Customer.builder()
                                                                     .firstname(firstname)
                                                                     .lastname(lastname)
                                                                     .email(email)
                                                                     .phone(phone)
                                                                     .password(password)
                                                                     .company(true)
                                                                     .vatin(vatin)
                                                                     .address(address)
                                                                     .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void doNotCreateCompanyWithInvalidVatin() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCompany);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Customer.builder()
                                                                     .firstname(firstname)
                                                                     .lastname(lastname)
                                                                     .email(email)
                                                                     .phone(phone)
                                                                     .password(password)
                                                                     .company(true)
                                                                     .cin(cin)
                                                                     .vatin("----")
                                                                     .address(address)
                                                                     .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void doNotCreateCompanyWithoutVatin() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCompany);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Customer.builder()
                                                                     .firstname(firstname)
                                                                     .lastname(lastname)
                                                                     .email(email)
                                                                     .phone(phone)
                                                                     .password(password)
                                                                     .company(true)
                                                                     .cin(cin)
                                                                     .address(address)
                                                                     .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void doNotCreateCompanyWithInvalidCin() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCompany);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Customer.builder()
                                                                     .firstname(firstname)
                                                                     .lastname(lastname)
                                                                     .email(email)
                                                                     .phone(phone)
                                                                     .password(password)
                                                                     .company(true)
                                                                     .cin("AaAaAaAaAaaAaaAAaa")
                                                                     .vatin(vatin)
                                                                     .address(address)
                                                                     .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void createValidCostumerTest() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Customer.builder()
                                                                     .firstname(firstname)
                                                                     .lastname(lastname)
                                                                     .email(email)
                                                                     .phone(phone)
                                                                     .password(password)
                                                                     .company(false)
                                                                     .address(address)
                                                                     .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id", is(id)))
               .andExpect(jsonPath("$.firstname", is(firstname)))
               .andExpect(jsonPath("$.lastname", is(lastname)))
               .andExpect(jsonPath("$.email", is(email)))
               .andExpect(jsonPath("$.phone", is(phone)))
               .andExpect(jsonPath("$.role", is(role)))
               .andExpect(jsonPath("$.company", is(false)))
               .andExpect(jsonPath("$.address.street", is(address.getStreet())))
               .andExpect(jsonPath("$.address.descriptionNumber", is(address.getDescriptionNumber())))
               .andExpect(jsonPath("$.address.city", is(address.getCity())))
               .andExpect(jsonPath("$.address.postcode", is(address.getPostcode())));
    }
    
    @Test
    public void doNotCreateCostumerWithCin() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Customer.builder()
                                                                     .firstname(firstname)
                                                                     .lastname(lastname)
                                                                     .email(email)
                                                                     .phone(phone)
                                                                     .password(password)
                                                                     .company(false)
                                                                     .cin(cin)
                                                                     .address(address)
                                                                     .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void doNotCreateCostumerWithVatin() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Customer.builder()
                                                                     .firstname(firstname)
                                                                     .lastname(lastname)
                                                                     .email(email)
                                                                     .phone(phone)
                                                                     .password(password)
                                                                     .company(false)
                                                                     .vatin(vatin)
                                                                     .address(address)
                                                                     .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void doNotCreateExistingCustomerTest() throws Exception {
        // Given
        String claimedMail = "customer@example.com";
        given(userRepository.existsByEmail(eq(claimedMail))).willReturn(true);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(claimedMail)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateCustomerWithInvalidMailTest() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email("@@@")
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateCustomerWithInvalidPhone() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone("7")
                                                                        .password(password)
                                                                        .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateCustomerWithEmptyPassword() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password("")
                                                                        .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateCustomerWithEmptyFirstName() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname("")
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateCustomerWithEmptyLastName() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname("")
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateCustomerWithForcedID() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .id(UUID.fromString(id))
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateCustomerWithNoParams() throws Exception {
        // Given
        given(customerRepository.save(any())).willReturn(createdCustomer);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .build());

        mockMvc.perform(post(URL_API).contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

}
