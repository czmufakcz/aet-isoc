package com.aetisoc.main;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.aetisoc.security.domain.Businessman;
import com.aetisoc.security.domain.ROLE;
import com.aetisoc.security.model.BusinessmanRepository;
import com.aetisoc.utils.BaseIntegrationTest;

public class BusinessmanRestControlerIntegrationTest extends BaseIntegrationTest {

    private final String URL_API = "/api/businessmans";

    private final String id = "2757b7bf-e149-4827-b615-3cc65c4e3d73";
    private final String firstname = "Bohumil";
    private final String lastname = "Novák";
    private final String email = "bohumil@example.com";
    private final String phone = "739444555";
    private final String password = "password";
    private final String role = "BUSINESSMAN";
    private final String job = "CEO";

    private final Businessman createdBusinessman = Businessman.builder()
                                                              .id(UUID.fromString(id))
                                                              .firstname(firstname)
                                                              .lastname(lastname)
                                                              .email(email)
                                                              .phone(phone)
                                                              .password(password)
                                                              .job(job)
                                                              .build();

    @MockBean
    private BusinessmanRepository businessmanRepository;

    @Test
    public void createValidBusinessmanTest() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id", is(id)))
               .andExpect(jsonPath("$.firstname", is(firstname)))
               .andExpect(jsonPath("$.lastname", is(lastname)))
               .andExpect(jsonPath("$.email", is(email)))
               .andExpect(jsonPath("$.phone", is(phone)))
               .andExpect(jsonPath("$.role", is(role)))
               .andExpect(jsonPath("$.job", is(job)));
    }

    @Test
    public void doNotCreateExistingBusinessmanTest() throws Exception {
        // Given
        String claimedMail = "businessman@example.com";
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(userRepository.existsByEmail(eq(claimedMail))).willReturn(true);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(claimedMail)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateBusinessmanWithInvalidMailTest() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email("@@@")
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateBusinessmanWithInvalidPhone() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone("7")
                                                                        .password(password)
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateBusinessmanWithEmptyPassword() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password("")
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateBusinessmanWithEmptyFirstName() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname("")
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateBusinessmanWithEmptyLastName() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname("")
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateBusinessmanWithEmptyJob() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .job("")
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateBusinessmanWithForcedID() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .id(UUID.fromString(id))
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotCreateBusinessmanWithNoParams() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    public void doNotCreateBusinessmanWithBussinessmanRights() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.BUSINESSMAN);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    public void doNotCreateBusinessmanWithCustomerRights() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
        given(businessmanRepository.save(any())).willReturn(createdBusinessman);

        // When and then
        String bodyRequest = objectMapper.writeValueAsString(Businessman.builder()
                                                                        .firstname(firstname)
                                                                        .lastname(lastname)
                                                                        .email(email)
                                                                        .phone(phone)
                                                                        .password(password)
                                                                        .job(job)
                                                                        .build());

        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .contentType(FORMAT)
                                     .content(bodyRequest)
                                     .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void returnBusinessmanTest() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.findById(eq(createdBusinessman.getId()))).willReturn(Optional.of(createdBusinessman));

        // When and then
        mockMvc.perform(get(URL_API + "/" + createdBusinessman.getId()
                                                              .toString()).header("Authorization", "Bearer " + accessToken)
                                                                          .accept(FORMAT))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id", is(id)))
               .andExpect(jsonPath("$.firstname", is(firstname)))
               .andExpect(jsonPath("$.lastname", is(lastname)))
               .andExpect(jsonPath("$.email", is(email)))
               .andExpect(jsonPath("$.phone", is(phone)))
               .andExpect(jsonPath("$.role", is(role)))
               .andExpect(jsonPath("$.job", is(job)));
    }

    @Test
    public void doNotReturnBusinessmanWithInvalidID() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.getOne(any())).willReturn(createdBusinessman);

        // When and then
        mockMvc.perform(get(URL_API + "/aaa").header("Authorization", "Bearer " + accessToken)
                                             .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotReturnBusinessmanWhoDoesNotExist() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);
        given(businessmanRepository.getOne(any())).willReturn(null);

        // When and then
        mockMvc.perform(get(URL_API + "/2757b7bf-e149-4827-b615-3cc65c4e3d74").header("Authorization", "Bearer " + accessToken)
                                                                              .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotReturnBusinessmanWithBussinessmanRights() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.BUSINESSMAN);
        given(businessmanRepository.getOne(any())).willReturn(createdBusinessman);

        // When and then
        mockMvc.perform(get(URL_API + "/" + id).header("Authorization", "Bearer " + accessToken)
                                               .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void doNotReturnBusinessmanWithCustomerRights() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
        given(businessmanRepository.getOne(any())).willReturn(createdBusinessman);

        // When and then
        mockMvc.perform(get(URL_API + "/" + id).header("Authorization", "Bearer " + accessToken)
                                               .accept(FORMAT))
               .andExpect(status().is4xxClientError());
    }

}
