package com.aetisoc.main;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static com.aetisoc.utils.FoundNTimesMatcher.containsNTimes;

import org.junit.Test;

import com.aetisoc.security.domain.ROLE;
import com.aetisoc.security.domain.User;
import com.aetisoc.utils.BaseIntegrationTest;

public class UserRestControllerIntegrationTest extends BaseIntegrationTest {

	private final String URL_API = "/api/users";
	
    @Test
    public void returnAllUsersTest() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);        
        // When and then
        mockMvc.perform(get(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .accept(FORMAT))
        			.andExpect(status().isOk())
        			.andExpect(content().string(containsNTimes("\"id\":", userRepository.count())));        			
    }
    
    @Test
    public void doNotReturnAllUsersWithBussinessmanRights() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.BUSINESSMAN);        
        // When and then
        mockMvc.perform(get(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .accept(FORMAT))
        		.andExpect(status().is4xxClientError()); 
    }

    @Test
    public void doNotReturnAllUsersWithCustomerRights() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.CUSTOMER);        
        // When and then
        mockMvc.perform(get(URL_API).header("Authorization", "Bearer " + accessToken)
                                     .accept(FORMAT))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void doNotReturnAllUsersWithNoRights() throws Exception { 
        // When and then
        mockMvc.perform(get(URL_API).accept(FORMAT))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void returnAdminUserForAdmin() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.ADMIN);   
        User loggedUser = userRepository.findByEmail(getUsername(ROLE.ADMIN)).get();
        
        // When and then
        mockMvc.perform(get(URL_API+"/whoLogged").header("Authorization", "Bearer " + accessToken)
                                     .accept(FORMAT))
        		.andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(loggedUser.getId().toString())))
                .andExpect(jsonPath("$.firstname", is(loggedUser.getFirstname())))
                .andExpect(jsonPath("$.lastname", is(loggedUser.getLastname())))
                .andExpect(jsonPath("$.email", is(loggedUser.getEmail())))
                .andExpect(jsonPath("$.phone", is(loggedUser.getPhone())))
                .andExpect(jsonPath("$.role", is(loggedUser.getRole().toString())));
    }
    
    @Test
    public void returnCustomerUserForCustomer() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.CUSTOMER);   
        User loggedUser = userRepository.findByEmail(getUsername(ROLE.CUSTOMER)).get();
        
        // When and then
        mockMvc.perform(get(URL_API+"/whoLogged").header("Authorization", "Bearer " + accessToken)
                                     .accept(FORMAT))
        		.andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(loggedUser.getId().toString())))
                .andExpect(jsonPath("$.firstname", is(loggedUser.getFirstname())))
                .andExpect(jsonPath("$.lastname", is(loggedUser.getLastname())))
                .andExpect(jsonPath("$.email", is(loggedUser.getEmail())))
                .andExpect(jsonPath("$.phone", is(loggedUser.getPhone())))
                .andExpect(jsonPath("$.role", is(loggedUser.getRole().toString())));
    }
    
    @Test
    public void returnBusinessmanUserForBusinessman() throws Exception {
        // Given
        String accessToken = obtainAccessToken(ROLE.BUSINESSMAN);   
        User loggedUser = userRepository.findByEmail(getUsername(ROLE.BUSINESSMAN)).get();
        
        // When and then
        mockMvc.perform(get(URL_API+"/whoLogged").header("Authorization", "Bearer " + accessToken)
                                     .accept(FORMAT))
        		.andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(loggedUser.getId().toString())))
                .andExpect(jsonPath("$.firstname", is(loggedUser.getFirstname())))
                .andExpect(jsonPath("$.lastname", is(loggedUser.getLastname())))
                .andExpect(jsonPath("$.email", is(loggedUser.getEmail())))
                .andExpect(jsonPath("$.phone", is(loggedUser.getPhone())))
                .andExpect(jsonPath("$.role", is(loggedUser.getRole().toString())));
    }
    
    @Test
    public void doNotReturnUserForNotLogged() throws Exception {
        // When and then
        mockMvc.perform(get(URL_API+"/whoLogged")
                                     .accept(FORMAT))
        		.andExpect(status().is4xxClientError());
    }
    
    
	
}
