package com.aetisoc.main;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.aetisoc.security.domain.ROLE;
import com.aetisoc.utils.BaseIntegrationTest;

public class SecurityIntegrationTest extends BaseIntegrationTest {

	private static final String URL_API = "/oauth/token";

    @Test
    public void adminCanLoginTest() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", getUsername(ROLE.ADMIN));
										        params.add("password", PASSWORD_LOGIN);

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().isOk())
        		.andExpect(content().contentType("application/json;charset=UTF-8"));
    }
    
    @Test
    public void adminCantLoginWithoutUsername() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("password", PASSWORD_LOGIN);

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void adminCantLoginWithWrongUsername() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", "adminator");
										        params.add("password", PASSWORD_LOGIN);

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void adminCantLoginWithoutPassword() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", getUsername(ROLE.ADMIN));

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void adminCantLoginWithWrongPassword() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", getUsername(ROLE.ADMIN));
										        params.add("password", "");

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void businessmanCanLoginTest() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", getUsername(ROLE.BUSINESSMAN));
										        params.add("password", PASSWORD_LOGIN);

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().isOk())
        		.andExpect(content().contentType("application/json;charset=UTF-8"));
    }
    
    @Test
    public void businessmanCantLoginWithoutUsername() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("password", PASSWORD_LOGIN);

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void businessmanCantLoginWithWrongUsername() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", "wrong");
										        params.add("password", PASSWORD_LOGIN);

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void businessmanCantLoginWithoutPassword() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", getUsername(ROLE.BUSINESSMAN));

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void businessmanCantLoginWithWrongPassword() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", getUsername(ROLE.BUSINESSMAN));
										        params.add("password", "");

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }

    @Test
    public void customerCanLoginTest() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", getUsername(ROLE.CUSTOMER));
										        params.add("password", PASSWORD_LOGIN);

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().isOk())
        		.andExpect(content().contentType("application/json;charset=UTF-8"));
    }
    
    @Test
    public void customerCantLoginWithoutUsername() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("password", PASSWORD_LOGIN);

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void customerCantLoginWithWrongUsername() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", "wrong");
										        params.add("password", PASSWORD_LOGIN);

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void customerCantLoginWithoutPassword() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", getUsername(ROLE.CUSTOMER));

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }
    
    @Test
    public void customerCantLoginWithWrongPassword() throws Exception {
        // When and then
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
										        params.add("grant_type", GRANT_TYPE);
										        params.add("username", getUsername(ROLE.CUSTOMER));
										        params.add("password", "");

        mockMvc.perform(post(URL_API).params(params)
                .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                .accept("application/json;charset=UTF-8"))
        		.andExpect(status().is4xxClientError());
    }

	
}
