package com.aetisoc.main;

import static com.aetisoc.utils.FoundNTimesMatcher.containsNTimes;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.aetisoc.receivable.domain.COMMODITY_TYPE;
import com.aetisoc.receivable.domain.RECEIVABLE_STATE;
import com.aetisoc.receivable.domain.Receivable;
import com.aetisoc.receivable.model.ReceivableRepository;
import com.aetisoc.security.domain.Customer;
import com.aetisoc.security.domain.ROLE;
import com.aetisoc.security.domain.User;
import com.aetisoc.security.model.Address;
import com.aetisoc.utils.BaseIntegrationTest;

import net.minidev.json.JSONObject;

public class ReceivableRestControllerTest extends BaseIntegrationTest {

	private final String URL_API = "/api/receivables";

	@MockBean
	private ReceivableRepository receivableRepository;
	    
	private Customer customer;
	private final String fromCity = "Pardubice";
	private final String toCity = "Hradec Králové";
	private final LocalDateTime departureDate = LocalDateTime.of(2019, 1, 1, 7, 0);
	private final LocalDateTime deliveryDate = LocalDateTime.of(2019, 1, 4, 7, 0);
	private final COMMODITY_TYPE commodity = COMMODITY_TYPE.COAL;
	private final int quantity = 10;
	private final RECEIVABLE_STATE state = RECEIVABLE_STATE.CREATED;
	
	private Receivable createdReceivable;
	
	private final String updatedFromCity = "Kolín";
	private final RECEIVABLE_STATE updatedState = RECEIVABLE_STATE.PROCESS;
	
	private Receivable updatedReceivable;
	
    private final Customer anotherCustomer = Customer.builder()
	           .id(UUID.fromString("2757b7bf-e149-4827-b615-3cc65c4e3d73"))
	           .firstname("Bohumil")
	           .lastname("Novák")
	           .email("bohumil@example.com")
	           .phone("739444555")
	           .password("password")
	           .company(false)
            .address(new Address(new Long(1), "Brnenskeho", "22", "Brno", "533 22"))
	           .build();
    
	private final Receivable anotherReceivable = new Receivable(new Long(1001), anotherCustomer, fromCity, 
			toCity, departureDate ,deliveryDate, null, commodity, quantity, state);
														
	
	@Before
	public void initTestData() {
		customer = (Customer)userRepository.findByEmail(getUsername(ROLE.CUSTOMER)).get();
		createdReceivable =  new Receivable(new Long(1000), customer, fromCity, 
				toCity, departureDate ,deliveryDate, null, commodity, quantity, state);
		updatedReceivable =  new Receivable(new Long(1000), customer, updatedFromCity, 
				toCity, departureDate ,deliveryDate, null, commodity, quantity, updatedState);
	}
	
	@Test
	public void createValidReceivableTest() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("fromCity", fromCity)
	        		.appendField("toCity", toCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().isOk())
	               .andExpect(jsonPath("$.customer.email", is(customer.getEmail())))
	               .andExpect(jsonPath("$.fromCity", is(fromCity)))
	               .andExpect(jsonPath("$.toCity", is(toCity)))
	               .andExpect(jsonPath("$.departureDate", containsString(departureDate.toString())))
	               .andExpect(jsonPath("$.deliveryDate", containsString(deliveryDate.toString())))
	               .andExpect(jsonPath("$.quantity", is(quantity)))
	               .andExpect(jsonPath("$.commodityType", is(commodity.toString())))
	               .andExpect(jsonPath("$.receivableState", is(state.toString())));
	}
	
	@Test
	public void doNotCreateReceivableWithAdminRights() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.ADMIN);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("fromCity", fromCity)
	        		.appendField("toCity", toCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithBusinessmanRights() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.BUSINESSMAN);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("fromCity", fromCity)
	        		.appendField("toCity", toCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithoutRights() throws Exception {
	        // Given		
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("fromCity", fromCity)
	        		.appendField("toCity", toCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithForcedId() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("id", "1500")
	        		.appendField("fromCity", fromCity)
	        		.appendField("toCity", toCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithDepartureLaterThanDelivery() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("fromCity", fromCity)
	        		.appendField("toCity", toCity)
	        		.appendField("departureDate", deliveryDate.toString())
	        		.appendField("deliveryDate",  departureDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithoutFromCity() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("toCity", toCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithoutToCity() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("fromCity", fromCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithoutDepartureDate() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("toCity", toCity)
	        		.appendField("fromCity", fromCity)
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithoutDeliveryDate() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("toCity", toCity)
	        		.appendField("fromCity", fromCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("commodityType", commodity.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithoutQuantity() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("toCity", toCity)
	        		.appendField("fromCity", fromCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("commodityType", commodity.toString());
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotCreateReceivableWithoutCommodity() throws Exception {
	        // Given		
	        String accessToken = obtainAccessToken(ROLE.CUSTOMER);
	        given(receivableRepository.save(any())).willReturn(createdReceivable);

	        // When and then
	        JSONObject rq = new JSONObject()
	        		.appendField("toCity", toCity)
	        		.appendField("fromCity", fromCity)
	        		.appendField("departureDate", departureDate.toString())
	        		.appendField("deliveryDate",  deliveryDate.toString())
	        		.appendField("quantity", quantity);
	        
	        
	        mockMvc.perform(post(URL_API).header("Authorization", "Bearer " + accessToken)
	                                     .contentType(FORMAT)
	                                     .content(rq.toString())
	                                     .accept(FORMAT))
	               .andExpect(status().is4xxClientError());
	}
	
	
	@Test
	public void returnAllCustomerReceivables() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.CUSTOMER);
		 User user = userRepository.findByEmail(getUsername(ROLE.CUSTOMER)).get();
		 
		 given(receivableRepository.findAllByCustomerId(user.getId()))
		 							.willReturn(Arrays.asList(createdReceivable));
		 
		 given(receivableRepository.findAllByCustomerId(anotherCustomer.getId()))
			.willReturn(Arrays.asList(anotherReceivable));
		 
		 // When and then
		 mockMvc.perform(get(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .accept(FORMAT))
		 	.andExpect(status().isOk())
		 	.andExpect(content().string(containsNTimes("\"customer\":", 1)));
	}
	
	@Test
	public void doNotreturnAllCustomerReceivablesWithAdminRights() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.ADMIN);
		 User user = userRepository.findByEmail(getUsername(ROLE.ADMIN)).get();
		 
		 given(receivableRepository.findAllByCustomerId(user.getId()))
		 							.willReturn(Arrays.asList(createdReceivable));
		 
		 given(receivableRepository.findAllByCustomerId(anotherCustomer.getId()))
			.willReturn(Arrays.asList(anotherReceivable));
		 
		 // When and then
		 mockMvc.perform(get(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .accept(FORMAT))
		 	.andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotreturnAllCustomerReceivablesWithBusinessRights() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.BUSINESSMAN);
		 User user = userRepository.findByEmail(getUsername(ROLE.BUSINESSMAN)).get();
		 
		 given(receivableRepository.findAllByCustomerId(user.getId()))
		 							.willReturn(Arrays.asList(createdReceivable));
		 
		 given(receivableRepository.findAllByCustomerId(anotherCustomer.getId()))
			.willReturn(Arrays.asList(anotherReceivable));
		 
		 // When and then
		 mockMvc.perform(get(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .accept(FORMAT))
		 	.andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotreturnAllCustomerReceivablesWithoutRights() throws Exception {
		// Given		
		 User user = userRepository.findByEmail(getUsername(ROLE.BUSINESSMAN)).get();
		 
		 given(receivableRepository.findAllByCustomerId(user.getId()))
		 							.willReturn(Arrays.asList(createdReceivable));
		 
		 given(receivableRepository.findAllByCustomerId(anotherCustomer.getId()))
			.willReturn(Arrays.asList(anotherReceivable));
		 
		 // When and then
		 mockMvc.perform(get(URL_API)
                 .contentType(FORMAT)
                 .accept(FORMAT))
		 	.andExpect(status().is4xxClientError());
	}
	
	@Test
	public void returnAllReceivables() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.BUSINESSMAN);		 
		 given(receivableRepository.findAll())
		 							.willReturn(Arrays.asList(createdReceivable, anotherReceivable));

		 // When and then
		 mockMvc.perform(get(URL_API+"/all").header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .accept(FORMAT))
		 	.andExpect(status().isOk())
		 	.andExpect(content().string(containsNTimes("\"customer\":", 2)));   
	}
	
	@Test
	public void doNotReturnAllReceivablesWithAdminRights() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.ADMIN);		 
		 given(receivableRepository.findAll())
		 							.willReturn(Arrays.asList(createdReceivable, anotherReceivable));

		 // When and then
		 mockMvc.perform(get(URL_API+"/all").header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .accept(FORMAT))
		 	.andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotReturnAllReceivablesWithCustomerRights() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.CUSTOMER);		 
		 given(receivableRepository.findAll())
		 							.willReturn(Arrays.asList(createdReceivable, anotherReceivable));

		 // When and then
		 mockMvc.perform(get(URL_API+"/all").header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .accept(FORMAT))
		 	.andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotReturnAllReceivablesWithoutRights() throws Exception {
		// Given			 
		 given(receivableRepository.findAll())
		 							.willReturn(Arrays.asList(createdReceivable, anotherReceivable));

		 // When and then
		 mockMvc.perform(get(URL_API+"/all")
                 .contentType(FORMAT)
                 .accept(FORMAT))
		 	.andExpect(status().is4xxClientError());
	}
	
	@Test
	public void updateReceivableWithCustomerRights() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 User user = userRepository.findByEmail(getUsername(ROLE.CUSTOMER)).get();
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate", departureDate.toString())
			        		.appendField("deliveryDate",  deliveryDate.toString())
			        		.appendField("commodityType", commodity.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 	.andExpect(status().isOk())
		 	.andExpect(jsonPath("$.id", is(createdReceivable.getId().intValue())))
	        .andExpect(jsonPath("$.fromCity", is(updatedFromCity)))
	        .andExpect(jsonPath("$.toCity", is(toCity)))
	        .andExpect(jsonPath("$.departureDate", containsString(departureDate.toString())))
	        .andExpect(jsonPath("$.deliveryDate", containsString(deliveryDate.toString())))
	        .andExpect(jsonPath("$.quantity", is(quantity)))
	        .andExpect(jsonPath("$.commodityType", is(commodity.toString())))
	        .andExpect(jsonPath("$.receivableState", is(updatedState.toString())))
		 	.andExpect(jsonPath("$.customer.id", is(user.getId().toString())))
			.andExpect(jsonPath("$.customer.email", is(customer.getEmail())));
	}
	
	@Test
	public void updateReceivableWithBussinessmanRights() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.BUSINESSMAN);	
		 User user = userRepository.findByEmail(getUsername(ROLE.CUSTOMER)).get();
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate", departureDate.toString())
			        		.appendField("deliveryDate",  deliveryDate.toString())
			        		.appendField("commodityType", commodity.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 	.andExpect(status().isOk())
		 	.andExpect(jsonPath("$.id", is(createdReceivable.getId().intValue())))
	        .andExpect(jsonPath("$.fromCity", is(updatedFromCity)))
	        .andExpect(jsonPath("$.toCity", is(toCity)))
	        .andExpect(jsonPath("$.departureDate", containsString(departureDate.toString())))
	        .andExpect(jsonPath("$.deliveryDate", containsString(deliveryDate.toString())))
	        .andExpect(jsonPath("$.quantity", is(quantity)))
	        .andExpect(jsonPath("$.commodityType", is(commodity.toString())))
	        .andExpect(jsonPath("$.receivableState", is(updatedState.toString())))
		 	.andExpect(jsonPath("$.customer.id", is(user.getId().toString())))
			.andExpect(jsonPath("$.customer.email", is(customer.getEmail())));
	}
	
	@Test
	public void doNotUpdateReceivableWithAdminRights() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.ADMIN);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate", departureDate.toString())
			        		.appendField("deliveryDate",  deliveryDate.toString())
			        		.appendField("commodityType", commodity.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 	.andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithoutRights() throws Exception {
		// Given		
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate", departureDate.toString())
			        		.appendField("deliveryDate",  deliveryDate.toString())
			        		.appendField("commodityType", commodity.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableOfOtherCustomer() throws Exception {
		// Given		
		 String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(anotherReceivable.getId())).willReturn(Optional.of(anotherReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", anotherReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate", departureDate.toString())
			        		.appendField("deliveryDate",  deliveryDate.toString())
			        		.appendField("commodityType", commodity.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithWrongId() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", "aa")
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", deliveryDate.toString())
			        		.appendField("commodityType", commodity)
			        		.appendField("quantity", 10)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithMissingId() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", deliveryDate.toString())
			        		.appendField("commodityType", commodity)
			        		.appendField("quantity", 10)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithMissingToCity() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", deliveryDate.toString())
			        		.appendField("commodityType", commodity)
			        		.appendField("quantity", 10)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	
	@Test
	public void doNotUpdateReceivableWithMissingFromCity() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", deliveryDate.toString())
			        		.appendField("commodityType", commodity)
			        		.appendField("quantity", 10)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithWrongDepartureDate() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate", "a")
			        		.appendField("deliveryDate",  deliveryDate.toString())
			        		.appendField("commodityType", commodity.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithMissingDepartureDate() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("deliveryDate",  deliveryDate.toString())
			        		.appendField("commodityType", commodity.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithWrongDeliveryDate() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", "a")
			        		.appendField("commodityType", commodity.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithMissingDeliveryDate() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("commodityType", commodity.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithWrongCommodityType() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", deliveryDate.toString())
			        		.appendField("commodityType", "a")
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWitMissingCommodityType() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", deliveryDate.toString())
			        		.appendField("quantity", quantity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithWrongQuantity() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", deliveryDate.toString())
			        		.appendField("commodityType", commodity)
			        		.appendField("quantity", -42)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithMissingQuantity() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", deliveryDate.toString())
			        		.appendField("commodityType", commodity)
	     					.appendField("receivableState", updatedState);
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
	@Test
	public void doNotUpdateReceivableWithWrongState() throws Exception {
		// Given	
		String accessToken = obtainAccessToken(ROLE.CUSTOMER);	
		 given(receivableRepository.findById(createdReceivable.getId())).willReturn(Optional.of(createdReceivable));
		 given(receivableRepository.save(any())).willReturn(updatedReceivable);
		 
		 // When and then
	     JSONObject rq = new JSONObject()
	    		 			.appendField("id", createdReceivable.getId())
			    		 	.appendField("toCity", toCity)
			        		.appendField("fromCity", updatedFromCity)
			        		.appendField("departureDate",  departureDate.toString())
			        		.appendField("deliveryDate", deliveryDate.toString())
			        		.appendField("commodityType", commodity)
			        		.appendField("quantity", 10)
	     					.appendField("receivableState", "a");
		 
		 mockMvc.perform(put(URL_API).header("Authorization", "Bearer " + accessToken)
                 .contentType(FORMAT)
                 .content(rq.toString())
                 .accept(FORMAT))
		 .andExpect(status().is4xxClientError());
	}
	
}
