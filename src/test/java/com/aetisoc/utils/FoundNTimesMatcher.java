package com.aetisoc.utils;

import org.hamcrest.CustomMatcher;
import org.springframework.util.StringUtils;

public class FoundNTimesMatcher extends CustomMatcher<String>{

	public static FoundNTimesMatcher containsNTimes(String pattern, long count) {
		return new FoundNTimesMatcher(pattern, count);
	}
	
	private final String pattern;
	private final long count;
	
	private FoundNTimesMatcher(String pattern, long count) {
		super("Containss "+pattern);
		this.pattern = pattern;
		this.count = count;
	}

	@Override
	public boolean matches(Object item) {
		if(item instanceof String) {
			return StringUtils.countOccurrencesOf((String)item, pattern) == count;
		}
		return false;
	}
	
}