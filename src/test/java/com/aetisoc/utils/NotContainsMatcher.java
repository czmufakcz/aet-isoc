package com.aetisoc.utils;

import org.hamcrest.CustomMatcher;

public class NotContainsMatcher extends CustomMatcher<String>{

	public static NotContainsMatcher notContains(String pattern) {
		return new NotContainsMatcher(pattern);
	}
	
	private final String pattern;
	
	private NotContainsMatcher(String pattern) {
		super("Containss "+pattern);
		this.pattern = pattern;
	}

	@Override
	public boolean matches(Object item) {
		if(item instanceof String) {
			return !((String)item).contains(pattern);
		}
		return true;
	}
	
}