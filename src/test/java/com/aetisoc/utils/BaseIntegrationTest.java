package com.aetisoc.utils;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.eq;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.aetisoc.AetIsocApplication;
import com.aetisoc.security.domain.Admin;
import com.aetisoc.security.domain.Businessman;
import com.aetisoc.security.domain.Customer;
import com.aetisoc.security.domain.ROLE;
import com.aetisoc.security.domain.User;
import com.aetisoc.security.model.UserRepository;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = AetIsocApplication.class)
public abstract class BaseIntegrationTest {

    protected static final String USERNAME_OAUTH = "aet";
    protected static final String PASSWORD_OAUTH = "isoc";
    protected static final String GRANT_TYPE = "password";
    protected static final String PASSWORD_LOGIN = "password";

    protected final String FORMAT = "application/json;charset=UTF-8";

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @MockBean
    protected UserRepository userRepository;

    protected MockMvc mockMvc;
    protected ObjectMapper objectMapper;

    public BaseIntegrationTest() {
        this.objectMapper = new ObjectMapper();
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                                      .addFilter(springSecurityFilterChain)
                                      .build();

        User admin = (User) Admin.builder()
                                 .id(UUID.fromString("808bc4b3-415d-47ea-9e3a-3ff715ddd99e"))
                                 .email("admin@example.com")
                                 .firstname("Admin")
                                 .lastname("AdminTest")
                                 .phone("111222333")
                                 .password("$2a$10$BzUjhYbePVPmmhbqM8kqp.0MaCVaBB3p4xMdPGoeKFO0dK2tAzVD6")
                                 .locked(false)
                                 .active(true)
                                 .build();
        User businessman = (User) Businessman.builder()
                .id(UUID.fromString("0914f4bd-6b9a-428f-9f50-aaca8676d5d7"))
                .email("businessman@example.com")
                .firstname("Businessman")
                .lastname("BusinessmanTest")
                .phone("444555666")
                .password("$2a$10$BzUjhYbePVPmmhbqM8kqp.0MaCVaBB3p4xMdPGoeKFO0dK2tAzVD6")
                .locked(false)
                .active(true)
                .job("DIRECTOR")
                .build();
        User customer = (User) Customer.builder()
                .id(UUID.fromString("ad59e945-f3ab-48c6-85a2-c2bb3c4d99c9"))
                .email("customer@example.com")
                .firstname("Customer")
                .lastname("CustomerTest")
                .phone("444555666")
                .password("$2a$10$BzUjhYbePVPmmhbqM8kqp.0MaCVaBB3p4xMdPGoeKFO0dK2tAzVD6")
                .locked(false)
                .active(true)
                .build();

        given(userRepository.findByEmail(eq("admin@example.com"))).willReturn(Optional.of(admin));
        given(userRepository.findByEmail(eq("businessman@example.com"))).willReturn(Optional.of(businessman));
        given(userRepository.findByEmail(eq("customer@example.com"))).willReturn(Optional.of(customer));
    }

    protected String obtainAccessToken(ROLE role) throws Exception {

        String username = getUsername(role);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", GRANT_TYPE);
        params.add("username", username);
        params.add("password", PASSWORD_LOGIN);

        ResultActions result = mockMvc.perform(post("/oauth/token").params(params)
                                                                   .with(httpBasic(USERNAME_OAUTH, PASSWORD_OAUTH))
                                                                   .accept("application/json;charset=UTF-8"))
                                      .andExpect(status().isOk())
                                      .andExpect(content().contentType("application/json;charset=UTF-8"));

        String resultString = result.andReturn()
                                    .getResponse()
                                    .getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString)
                         .get("access_token")
                         .toString();
    }

    protected String getUsername(ROLE role) {
        switch (role) {
        case ADMIN:
            return "admin@example.com";
        case CUSTOMER:
            return "customer@example.com";
        case BUSINESSMAN:
            return "businessman@example.com";
        default:
            throw new IllegalArgumentException("Wrong role.");
        }
    }

}
