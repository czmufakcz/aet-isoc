package com.aetisoc.offers;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.aetisoc.calculator.Calculator;
import com.aetisoc.offer.domain.OFFER_STATE;
import com.aetisoc.offer.domain.Offer;
import com.aetisoc.offer.domain.Resources;
import com.aetisoc.offer.domain.TRAIN_TYPE;
import com.aetisoc.offer.model.OfferService;
import com.aetisoc.path.generator.Path;
import com.aetisoc.path.generator.PathService;
import com.aetisoc.receivable.domain.COMMODITY_TYPE;
import com.aetisoc.receivable.domain.RECEIVABLE_STATE;
import com.aetisoc.receivable.domain.Receivable;
import com.aetisoc.receivable.model.ReceivableService;
import com.aetisoc.security.domain.Businessman;
import com.aetisoc.security.domain.Customer;
import com.aetisoc.security.domain.ROLE;
import com.aetisoc.security.model.BusinessmanService;
import com.aetisoc.security.model.CustomerService;
import com.aetisoc.utils.BaseIntegrationTest;

public class OfferServiceTest extends BaseIntegrationTest {
	
	@Autowired
	private OfferService offerService;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private ReceivableService receivableService;
	@Autowired
	private BusinessmanService businessmanService;
	@Autowired
	private PathService pathService;
	@Autowired
	private Calculator calculator;
		
	private Receivable createdReceivable;
	private Offer createdOffer;
	
	
	@Before
	@Rollback
	public void reserveConnections() throws Exception {
		Businessman bussinesman = businessmanService.findBusinessmanByID(UUID.fromString("ad59e945-f3ab-48c6-85a2-c2bb3c4d99c9"));
		Customer customer = customerService.findCustomerByID(UUID.fromString("0914f4bd-6b9a-428f-9f50-aaca8676d5d7"));
		
	    createdReceivable = Receivable.builder()
										.customer(customer)
										.quantity(200)
										.commodityType(COMMODITY_TYPE.COAL)
										.fromCity("Pardubice")
										.toCity("Přelouč")
										.departureDate(LocalDateTime.of(2018, 12, 1, 0, 0))
										.deliveryDate(LocalDateTime.of(2018, 12, 2, 0, 0))
										.build();
	    createdReceivable.setReceivableState(RECEIVABLE_STATE.ACCEPTED_BY_CUSTOMER);
	    createdReceivable = receivableService.save(createdReceivable);
	    	    
	    createdOffer = Offer.builder()
	    					.original(createdReceivable)
	    					.businessman(bussinesman)
	    					.priceResource(250.0)
	    					.departureDate(LocalDateTime.of(2018, 12, 1, 0, 0))
	    					.deliveryDate(LocalDateTime.of(2018, 12, 2, 0, 0))
	    					.paths(pathService.generatePaths("Pardubice", "Přelouč", 1, LocalDateTime.of(2018, 12, 1, 0, 0), LocalDateTime.of(2018, 12, 2, 0, 0)))
	    					.resources(Resources.builder().train(TRAIN_TYPE.T25).wagons(calculator.getUsedWagons(COMMODITY_TYPE.COAL, 200)).build())
	    					.build();
	    
	    createdOffer.getPaths().get(0).setAccepted(true);
	    createdOffer.setOfferState(OFFER_STATE.ACCEPTED_BY_CUSTOMER);
	    
	    createdOffer = offerService.save(createdOffer);
	}
	
	@Test
	@Transactional
	public void testQueryingReservedConnectionBanwidth() throws Exception {
		obtainAccessToken(ROLE.ADMIN);
		
		List<Path> availablePaths = pathService.generatePaths("Pardubice", "Přelouč", 1, 
				LocalDateTime.of(2018, 12, 1, 0, 0), LocalDateTime.of(2018, 12, 2, 0, 0));
		assertEquals(0, availablePaths.size());
	}
	
}
