package com.aetisoc.pdf;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.junit.Test;

import com.aetisoc.calculator.PDFGenerator;
import com.aetisoc.offer.domain.Connection;
import com.aetisoc.offer.domain.Offer;
import com.aetisoc.offer.domain.Resources;
import com.aetisoc.offer.domain.TRAIN_TYPE;
import com.aetisoc.offer.domain.UNIT_TYPE;
import com.aetisoc.offer.domain.Wagon;
import com.aetisoc.path.generator.Path;
import com.aetisoc.receivable.domain.COMMODITY_TYPE;
import com.aetisoc.receivable.domain.Receivable;
import com.aetisoc.security.domain.Customer;
import com.aetisoc.security.model.Address;

public class PDFGeneratorTest {

    private final String id = "2757b7bf-e149-4827-b615-3cc65c4e3d73";
    private final String firstname = "Bohumil";
    private final String lastname = "Novák";
    private final String email = "bohumil@example.com";
    private final String phone = "739444555";
    private final String password = "password";
    private final Address address = new Address(new Long(1), "Brnenskeho", "22", "Brno", "533 22");

    private Customer createdCustomer = Customer.builder()
                                               .id(UUID.fromString(id))
                                               .firstname(firstname)
                                               .lastname(lastname)
                                               .email(email)
                                               .phone(phone)
                                               .password(password)
                                               .company(false)
                                               .address(address)
                                               .build();

    Wagon wagon = Wagon.builder()
                       .identification("546654-445")
                       .loadCapacity(40)
                       .unitType(UNIT_TYPE.M3_SOLID)
                       .lease(300)
                       .build();
    Wagon wagon1 = Wagon.builder()
                        .id(2)
                        .identification("546654-888")
                        .loadCapacity(30)
                        .unitType(UNIT_TYPE.M3_SOLID)
                        .lease(600)
                        .build();
    List<Wagon> wagons = new LinkedList<>();
    List<Connection> connections = new LinkedList<>();
    List<Path> paths = new LinkedList<>();

    Receivable receivable = Receivable.builder()
                                      .customer(createdCustomer)
                                      .commodityType(COMMODITY_TYPE.COAL)
                                      .quantity(350)
                                      .fromCity("Pardubice")
                                      .toCity("Praha")
                                      .build();
    Resources resources = Resources.builder()
                                   .train(TRAIN_TYPE.T10)
                                   .wagons(wagons)
                                   .build();
    Connection connection = Connection.builder()
                                      .duration(30)
                                      .build();
    Path path = Path.builder()
                    .accepted(true)
                    .connections(connections)
                    .build();

    Offer offer = Offer.builder()
                       .original(receivable)
                       .resources(resources)
                       .priceResource(564654.0)
                       .paths(paths)
                       .deliveryDate(LocalDateTime.parse("2017-04-08T12:30:20"))
                       .build();

    @Test
    public void PDFGen() {
        wagons.add(wagon);
        wagons.add(wagon);
        wagons.add(wagon1);
        paths.add(path);
        connections.add(connection);

        PDFGenerator.generatePDF(offer);
    }
}
