package com.aetisoc.receivable.domain;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class CommodityTypeSerializer extends StdSerializer<COMMODITY_TYPE>{

    private static final long serialVersionUID = 4221727328591073116L;
    
    public CommodityTypeSerializer() {
        super(COMMODITY_TYPE.class);
    }
    
    public CommodityTypeSerializer(Class<COMMODITY_TYPE> t) {
        super(t);
    }
    
    @Override
    public void serialize(COMMODITY_TYPE commodity, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeFieldName("name");
        gen.writeString(commodity.name());
        gen.writeFieldName("translate");
        gen.writeString(commodity.getTranslate());
        gen.writeFieldName("price");
        gen.writeNumber(commodity.getPrice());
        gen.writeEndObject();
        
    }

    
}
