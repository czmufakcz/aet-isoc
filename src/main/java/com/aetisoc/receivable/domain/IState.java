package com.aetisoc.receivable.domain;

public interface IState {

    String name();
    
    String getTranslate();
}
