package com.aetisoc.receivable.domain;

import com.aetisoc.offer.domain.UNIT_TYPE;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
@JsonSerialize(using = CommodityTypeSerializer.class)
public enum COMMODITY_TYPE {
    COAL("uhlí"), CARS("automobil"), SAND("písek"), FUEL("benzín"), 
    WOOD("dřevo"), GRAVEL("štěrk"), OX("dobytek"), MINERALS("minerály"),
    STONE("kámen"), METAL("kovy"), VALUABLE_METAL("cenné kovy"), 
    ELECTRONICS("elektronika"), BIOMASS("biomasa"), POULTRY("drůbež"),
    FABRIC("tkaniny"), CHEMICALS("chemikálie"), GAS("plyny"), LIQUIDS("kapaliny"),
    FROZEN_FOOD("mražené potraviny"), FOOD("nekazící se potraviny");

    private final String translate;

    public double getPrice() {
        switch (this) {
        case COAL:
            return 300;
        case CARS:
            return 400;
        case SAND:
            return 300;
        case FUEL:
            return 300;
        case WOOD:
            return 250;
        case GRAVEL:
            return 300;
        case OX:
            return 350;
        case MINERALS:
            return 400;
        case STONE:
            return 200;
        case METAL:
            return 275;
        case VALUABLE_METAL:
            return 600;
        case ELECTRONICS:
            return 350;
        case BIOMASS:
            return 150;
        case POULTRY:
            return 300;
        case FABRIC:
            return 200;
        case CHEMICALS:
            return 400;
        case GAS:
            return 285;
        case LIQUIDS:
            return 175;
        case FROZEN_FOOD:
            return 225;
        case FOOD:
            return 150;
        default:
            throw new IllegalArgumentException("Wrong WAGON_TYPE.");
        }
    }

    public UNIT_TYPE getUnitType() {
        switch (this) {
        case COAL:
            return UNIT_TYPE.M3_SOLID;
        case CARS:
            return UNIT_TYPE.M3_SOLID;
        case SAND:
            return UNIT_TYPE.M3_SOLID;
        case FUEL:
            return UNIT_TYPE.M3_LIQUID;
        case WOOD:
            return UNIT_TYPE.M3_SOLID;
        case GRAVEL:
            return UNIT_TYPE.M3_SOLID;
        case OX:
            return UNIT_TYPE.M3_SOLID;
        case MINERALS:
            return UNIT_TYPE.M3_SOLID;
        case STONE:
            return UNIT_TYPE.M3_SOLID;
        case METAL:
            return UNIT_TYPE.M3_SOLID;
        case VALUABLE_METAL:
           return UNIT_TYPE.M3_SOLID;
        case ELECTRONICS:
            return UNIT_TYPE.M3_SOLID;
        case BIOMASS:
             return UNIT_TYPE.M3_LIQUID;
        case POULTRY:
            return UNIT_TYPE.M3_SOLID;
        case FABRIC:
            return UNIT_TYPE.M3_SOLID;
        case CHEMICALS:
             return UNIT_TYPE.M3_LIQUID;
        case GAS:
             return UNIT_TYPE.M3_LIQUID;
        case LIQUIDS:
             return UNIT_TYPE.M3_LIQUID;
        case FROZEN_FOOD:
            return UNIT_TYPE.M3_SOLID;
        case FOOD:
           return UNIT_TYPE.M3_SOLID;
        default:
            throw new IllegalArgumentException("Wrong WAGON_TYPE.");
        }
    }

}
