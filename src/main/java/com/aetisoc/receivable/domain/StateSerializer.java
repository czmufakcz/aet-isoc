package com.aetisoc.receivable.domain;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class StateSerializer extends StdSerializer<IState> {

    private static final long serialVersionUID = -6373771872843105882L;

    public StateSerializer() {
        super(IState.class);
    }

    public StateSerializer(Class<IState> t) {
        super(t);
    }

    @Override
    public void serialize(IState state, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeFieldName("name");
        gen.writeString(state.name());
        gen.writeFieldName("translate");
        gen.writeString(state.getTranslate());
        gen.writeEndObject();

    }
}
