package com.aetisoc.receivable.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.annotations.CreationTimestamp;

import com.aetisoc.security.domain.Customer;
import com.aetisoc.validators.ValidDate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
@EqualsAndHashCode
@ValidDate
@Entity
@Table(name = "receivables")
public class Receivable implements IDeliveryRange {

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "receivables_id_seq", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;
    @NotEmpty
    private String fromCity;
    @NotEmpty
    private String toCity;
    @NotNull
    private LocalDateTime departureDate;
    @NotNull
    private LocalDateTime deliveryDate;
    @CreationTimestamp
    private LocalDateTime createdAt;
    @NotNull
    @Enumerated(EnumType.STRING)
    private COMMODITY_TYPE commodityType;
    @Positive
    private int quantity;
    @Enumerated(EnumType.STRING)
    private RECEIVABLE_STATE receivableState;

}
