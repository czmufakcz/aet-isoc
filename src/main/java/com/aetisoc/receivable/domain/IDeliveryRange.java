package com.aetisoc.receivable.domain;

import java.time.LocalDateTime;

public interface IDeliveryRange {

    LocalDateTime getDepartureDate();

    LocalDateTime getDeliveryDate();
}
