package com.aetisoc.receivable.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
@JsonSerialize(using = StateSerializer.class)
public enum RECEIVABLE_STATE implements IState {
    CREATED("vytvořeno"), PROCESS("zpracovává se"), DECLINED_BY_BUSINESSMAN("zamítnuto obchodníkem"), CANCELED_BY_CUSTOMER("zrušeno zákazníkem"), ACCEPTED_BY_CUSTOMER("schváleno zákazníkem"), DELIVERED("doručeno");

    private final String translate;
}
