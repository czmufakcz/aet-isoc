package com.aetisoc.receivable.model;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.aetisoc.receivable.domain.RECEIVABLE_STATE;
import com.aetisoc.receivable.domain.Receivable;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class ReceivableService {

    private final ReceivableRepository receivableRepository;

    public Receivable save(Receivable receivable) {
        Receivable savedReceivable = receivableRepository.save(receivable);
        log.info("Successfully to saved receivable to DB: {}", savedReceivable);
        return savedReceivable;
    }

    public Collection<Receivable> findAllByReceivableState(RECEIVABLE_STATE receivableState) {
        return receivableRepository.findAllByReceivableState(receivableState);
    }

    public Collection<Receivable> findAllByReceivableStateAndCustomerId(UUID customerID, RECEIVABLE_STATE receivableState) {
        return receivableRepository.findAllByReceivableState(receivableState)
                                   .stream()
                                   .filter(receivable -> receivable.getCustomer()
                                                                   .getId()
                                                                   .equals(customerID))
                                   .collect(Collectors.toList());
    }

    public Optional<Receivable> findByIdReceivable(Long id) {
        return receivableRepository.findById(id);
    }

    public Collection<Receivable> findAllByCustomerID(UUID uuid) {
        return receivableRepository.findAllByCustomerId(uuid);
    }

    public Collection<Receivable> findAll() {
        return receivableRepository.findAll();
    }

    public void delete(Long id) {
        receivableRepository.deleteById(id);
    }

    public Collection<Receivable> getReceivableBetween(LocalDateTime startDate, LocalDateTime endDate, RECEIVABLE_STATE receivableState) {
        return receivableRepository.findAllByCreatedAtBetweenAndReceivableState(startDate, endDate, receivableState);
    }

}
