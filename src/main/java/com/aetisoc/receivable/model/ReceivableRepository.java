package com.aetisoc.receivable.model;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aetisoc.receivable.domain.RECEIVABLE_STATE;
import com.aetisoc.receivable.domain.Receivable;

public interface ReceivableRepository extends JpaRepository<Receivable, Long> {

    Collection<Receivable> findAllByCustomerId(UUID uuid);

    Collection<Receivable> findAllByCreatedAtBetweenAndReceivableState(LocalDateTime startDate, LocalDateTime endDate, RECEIVABLE_STATE receivableState);

    Collection<Receivable> findAllByReceivableState(RECEIVABLE_STATE receivableState);
}