package com.aetisoc.receivable.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.aetisoc.offer.model.ConnectionService;
import com.aetisoc.receivable.domain.COMMODITY_TYPE;
import com.aetisoc.receivable.domain.RECEIVABLE_STATE;
import com.aetisoc.receivable.domain.Receivable;
import com.aetisoc.receivable.model.ReceivableService;
import com.aetisoc.security.domain.Customer;
import com.aetisoc.security.domain.ROLE;
import com.aetisoc.security.domain.User;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/receivables")
public class ReceivableRestController {

    private final ReceivableService receivableService;
    private final ConnectionService connectionService;

    @PostMapping
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @Transactional
    public Receivable createReceivable(@Valid @RequestBody Receivable receivable, @AuthenticationPrincipal Customer customer) throws Exception {
        if (receivable.getId() != null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID must be null.");
        validationCities(receivable.getFromCity(), receivable.getToCity());

        receivable.setCustomer(customer);
        receivable.setReceivableState(RECEIVABLE_STATE.CREATED);
        return receivableService.save(receivable);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @Transactional(readOnly = true)
    public Collection<Receivable> getReceivablesByCustomerId(@AuthenticationPrincipal Customer customer) throws Exception {
        return receivableService.findAllByCustomerID(customer.getId());
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_BUSINESSMAN')")
    @Transactional(readOnly = true)
    public Collection<Receivable> getReceivables() throws Exception {
        return receivableService.findAll();
    }
    
    @GetMapping("/{state}")
    @PreAuthorize("hasRole('ROLE_CUSTOMER') OR hasRole('ROLE_BUSINESSMAN')")
    @Transactional(readOnly = true)
    public Collection<Receivable> getReceivableByState(@PathVariable RECEIVABLE_STATE state, @AuthenticationPrincipal User user) throws Exception {
        if (user.getRole() == ROLE.BUSINESSMAN)
            return receivableService.findAllByReceivableState(state);
        else
            return receivableService.findAllByReceivableStateAndCustomerId(user.getId(), state);
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_CUSTOMER') OR hasRole('ROLE_BUSINESSMAN')")
    @Transactional
    public Receivable updateReceivable(@Valid @RequestBody Receivable receivable, @AuthenticationPrincipal User user) throws Exception {
        Receivable findReceivable = receivableService.findByIdReceivable(receivable.getId())
                                                     .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Receivable not found."));

        checkAccessToReceivable(findReceivable, user);
        validationCities(receivable.getFromCity(), receivable.getToCity());

        Receivable receivableToSave = Receivable.builder()
                                                .id(receivable.getId())
                                                .customer(findReceivable.getCustomer())
                                                .fromCity(receivable.getFromCity())
                                                .toCity(receivable.getToCity())
                                                .departureDate(receivable.getDepartureDate())
                                                .deliveryDate(receivable.getDeliveryDate())
                                                .commodityType(receivable.getCommodityType())
                                                .quantity(receivable.getQuantity())
                                                .receivableState(receivable.getReceivableState() == null ? findReceivable.getReceivableState() : receivable.getReceivableState())
                                                .build();

        return receivableService.save(receivableToSave);
    }

    @PreAuthorize("hasRole('ROLE_CUSTOMER') OR hasRole('ROLE_BUSINESSMAN')")
    @DeleteMapping(path = "/{id}")
    @Transactional
    public void deleteReceivable(@PathVariable Long id, @AuthenticationPrincipal User user) {
        Receivable findReceivable = receivableService.findByIdReceivable(id)
                                                     .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Receivable not found."));

        checkAccessToReceivable(findReceivable, user);
        receivableService.delete(id);
    }

    @GetMapping(path = "/commodities")
    @Transactional(readOnly = true)
    public Collection<COMMODITY_TYPE> getCommodities() {
        return new ArrayList<COMMODITY_TYPE>(EnumSet.allOf(COMMODITY_TYPE.class));
    }

    @GetMapping(path = "/fromCities")
    @Transactional(readOnly = true)
    public Collection<String> getConnectionsByFromCity() {
        return connectionService.findDistinctFromCity();
    }

    @GetMapping(path = "/toCities")
    @Transactional(readOnly = true)
    public Collection<String> getConnectionsByToCity() {
        return connectionService.findDistinctToCity();
    }

    private void checkAccessToReceivable(Receivable receivable, User user) {
        if (user.getRole() != ROLE.BUSINESSMAN && !receivable.getCustomer()
                                                             .getId()
                                                             .equals(user.getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This is not your receivable.");
    }

    private void validationCities(String fromCity, String toCity) {
        if (!connectionService.isExistFromCity(fromCity))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "From city not supported.");
        if (!connectionService.isExistToCity(toCity))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "To city not supported.");
    }

}
