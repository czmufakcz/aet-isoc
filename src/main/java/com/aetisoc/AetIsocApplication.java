package com.aetisoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AetIsocApplication {

    public static void main(String[] args) {
        SpringApplication.run(AetIsocApplication.class, args);
    }
}
