package com.aetisoc.calculator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import com.aetisoc.offer.domain.Offer;
import com.aetisoc.offer.domain.Resources;
import com.aetisoc.offer.domain.Wagon;
import com.aetisoc.path.generator.Path;
import com.aetisoc.security.domain.Customer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PDFGenerator {

    private static final int[] colorRGB = { 173, 173, 173 };
    private static final String[] headerTable = { "Číslo vozu", "Ložná hmotnost", "Počet náprav", "Cena za nápravu", "Cena" };

    private static final int PADDING_PAGE = 50;
    private static final int LINE_HEIGHT_14 = 18;

    private static final int BOX_WIDTH = 225;
    private static final int BOX_HEIGHT = 160;
    private static final int PADDING_BOX_WIDTH = 20;
    private static final int PADDING_BOX_HEIGHT = 30;

    private static float heightTable = 0;

    public static File generatePDF(Offer offer) {
        try {

            // BASE SETTINGS
            PDDocument document = new PDDocument();
            PDPage page = new PDPage(PDRectangle.A4);
            PDRectangle rect = page.getMediaBox();
            // FONTS
            InputStream robotoLight = PDFGenerator.class.getResourceAsStream("/fonts/Roboto-Light.ttf");
            PDType0Font fontNormal = PDType0Font.load(document, robotoLight, true);
            InputStream robotoBold = PDFGenerator.class.getResourceAsStream("/fonts/Roboto-Medium.ttf");
            PDType0Font fontBold = PDType0Font.load(document, robotoBold, true);

            document.addPage(page);

            PDPageContentStream contentStream = new PDPageContentStream(document, page);
            createSuplierBox(contentStream, rect, fontBold, fontNormal);
            createRecipientBox(contentStream, rect, fontBold, fontNormal, offer.getOriginal()
                                                                               .getCustomer());

            createHeaderInfo(contentStream, rect, fontBold, fontNormal, offer);
            createHeaderResources(contentStream, rect, fontBold);
            createTable(fontBold, fontNormal, contentStream, rect.getHeight() - 350, PADDING_PAGE, rect, offer.getResources());
            createFooterWithPrice(contentStream, rect, fontNormal, fontBold, offer);

            contentStream.close();
            File file = new File("generatePDF/Nabídka.pdf");
            document.save(file);
            document.close();
            
            return file;
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        throw new IllegalArgumentException("PDF generate failed.");

    }

    private static void createSuplierBox(PDPageContentStream contentStream, PDRectangle rect, PDType0Font fontBold, PDType0Font fontNormal) throws IOException {
        contentStream.setStrokingColor(colorRGB[0], colorRGB[1], colorRGB[2]);
        contentStream.addRect(PADDING_PAGE, rect.getHeight() - PADDING_PAGE - BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
        contentStream.closeAndStroke();

        contentStream.beginText();
        contentStream.setFont(fontBold, 10);
        contentStream.setLeading(LINE_HEIGHT_14);
        contentStream.newLineAtOffset(PADDING_PAGE + PADDING_BOX_WIDTH, rect.getHeight() - PADDING_PAGE - PADDING_BOX_HEIGHT);
        contentStream.showText("Dodavatel:");
        contentStream.newLine();
        contentStream.setFont(fontBold, 14);
        contentStream.showText("AET s.r.o");
        contentStream.newLine();
        contentStream.setFont(fontNormal, 12);
        contentStream.showText("Třída míru 55");
        contentStream.newLine();
        contentStream.showText("530 02 Pardubice");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("DIČ: 54646464     IČO: 65464654");
        contentStream.newLine();
        contentStream.showText("tel: +420 739 555 666");
        contentStream.endText();
    }

    private static void createRecipientBox(PDPageContentStream contentStream, PDRectangle rect, PDType0Font fontBold, PDType0Font fontNormal, Customer customer) throws IOException {
        contentStream.setStrokingColor(colorRGB[0], colorRGB[1], colorRGB[2]);
        contentStream.addRect(rect.getWidth() - PADDING_PAGE - BOX_WIDTH, rect.getHeight() - PADDING_PAGE - BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
        contentStream.closeAndStroke();

        contentStream.beginText();
        contentStream.setFont(fontBold, 10);
        contentStream.setLeading(LINE_HEIGHT_14);
        contentStream.newLineAtOffset(rect.getWidth() - PADDING_PAGE + PADDING_BOX_WIDTH - BOX_WIDTH, rect.getHeight() - PADDING_PAGE - PADDING_BOX_HEIGHT);
        contentStream.showText("Příjemce:");
        contentStream.newLine();
        contentStream.setFont(fontBold, 14);
        contentStream.showText(customer.getFirstname() + " " + customer.getLastname());
        contentStream.newLine();
        contentStream.setFont(fontNormal, 12);
        contentStream.showText(customer.getAddress()
                                       .getStreet() + " " + customer.getAddress()
                                                                    .getDescriptionNumber());
        contentStream.newLine();
        contentStream.showText(customer.getAddress()
                                       .getPostcode() + " " + customer.getAddress()
                                                                      .getCity());
        contentStream.newLine();
        contentStream.newLine();
        if (customer.isCompany())
            contentStream.showText("DIČ: " + customer.getCin() + "      IČO: " + customer.getVatin());
        contentStream.newLine();
        contentStream.showText("tel: +420 739 555 666");
        contentStream.endText();
    }

    private static void createHeaderInfo(PDPageContentStream contentStream, PDRectangle rect, PDType0Font fontBold, PDType0Font fontNormal, Offer offer) throws IOException {
        int countItems = 5;
        float widthCell = (rect.getWidth() - 2 * PADDING_PAGE) / countItems;
        float yPos = rect.getHeight() - 250 - (LINE_HEIGHT_14 + LINE_HEIGHT_14 / 2);

        contentStream.moveTo(PADDING_PAGE, rect.getHeight() - 260);
        contentStream.lineTo(rect.getWidth() - PADDING_PAGE, rect.getHeight() - 260);
        contentStream.stroke();

        contentStream.beginText();
        contentStream.newLineAtOffset(PADDING_PAGE, rect.getHeight() - 250);
        contentStream.setFont(fontBold, 16);
        contentStream.showText("Přehled objednávky:");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.endText();

        contentStream.setFont(fontBold, 10);

        contentStream.beginText();
        contentStream.newLineAtOffset(PADDING_PAGE, yPos);
        contentStream.showText("Odkud: " + offer.getOriginal()
                                                .getFromCity());
        contentStream.endText();

        contentStream.beginText();
        contentStream.newLineAtOffset(PADDING_PAGE + widthCell, yPos);
        contentStream.showText("Kam: " + offer.getOriginal()
                                              .getToCity());
        contentStream.endText();

        contentStream.beginText();
        contentStream.newLineAtOffset(PADDING_PAGE + widthCell * 2, yPos);
        contentStream.showText("Lokomotiva: " + offer.getResources()
                                                     .getTrain());
        contentStream.endText();

        contentStream.beginText();
        contentStream.newLineAtOffset(PADDING_PAGE + widthCell * 3, yPos);
        contentStream.showText("Komodita: " + offer.getOriginal()
                                                   .getCommodityType()
                                                   .getTranslate());
        contentStream.endText();

        contentStream.beginText();
        contentStream.newLineAtOffset(PADDING_PAGE + widthCell * 4, yPos);
        contentStream.showText("Množství: " + offer.getOriginal()
                                                   .getQuantity());
        contentStream.endText();

    }

    private static void createHeaderResources(PDPageContentStream contentStream, PDRectangle rect, PDType0Font fontBold) throws IOException {
        contentStream.beginText();
        contentStream.newLineAtOffset(PADDING_PAGE, rect.getHeight() - 330);
        contentStream.setFont(fontBold, 16);
        contentStream.showText("Přehled zdrojů:");
        contentStream.endText();

        contentStream.moveTo(PADDING_PAGE, rect.getHeight() - 340);
        contentStream.lineTo(rect.getWidth() - PADDING_PAGE, rect.getHeight() - 340);
        contentStream.stroke();

    }

    private static void createFooterWithPrice(PDPageContentStream contentStream, PDRectangle rect, PDType0Font fontNormal, PDType0Font fontBold, Offer offer) throws IOException {
        float yPost = rect.getHeight() - 350 - heightTable - LINE_HEIGHT_14 * 4;
        contentStream.beginText();
        contentStream.setFont(fontNormal, 12);
        contentStream.newLineAtOffset(PADDING_PAGE, yPost);
        contentStream.showText("Předpodkládané datum dodání: " + offer.getDeliveryDate()
                                                                      .format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        contentStream.endText();

        Path acceptedPath = offer.getPaths()
                                 .stream()
                                 .filter(p -> p.isAccepted())
                                 .findFirst()
                                 .get();

        NumberFormat numberFormat = new DecimalFormat("###,###.### 'Kč'");

        int fontSize = 12;
        String price = "Cena za pronajatí traťě:     " + numberFormat.format(acceptedPath.getPriceByConnections());
        float titleWidth = fontNormal.getStringWidth(price) / 1000 * fontSize;

        contentStream.beginText();
        contentStream.setFont(fontNormal, fontSize);
        contentStream.newLineAtOffset(rect.getWidth() - titleWidth - PADDING_PAGE, yPost + LINE_HEIGHT_14 + LINE_HEIGHT_14);
        contentStream.showText(price);
        contentStream.endText();

        fontSize = 16;
        price = "Cena:     " + numberFormat.format(offer.getFinalPrice());
        titleWidth = fontBold.getStringWidth(price) / 1000 * fontSize;

        contentStream.beginText();
        contentStream.setFont(fontBold, fontSize);
        contentStream.newLineAtOffset(rect.getWidth() - titleWidth - PADDING_PAGE, yPost);
        contentStream.showText(price);
        contentStream.endText();

    }

    public static void createTable(PDType0Font fontBold, PDType0Font fontNormal, PDPageContentStream contentStream, float y, float margin, PDRectangle rect, Resources resources) throws IOException {
        Map<Wagon, Long> correctlyWagons = getWagonsCorrectly(resources.getWagons());
        final int rows = correctlyWagons.size();
        final int cols = headerTable.length;
        final float rowHeight = 25f;
        final float tableWidth = rect.getWidth() - (2 * margin);
        final float colWidth = tableWidth / (float) cols;
        final float cellMargin = 5f;
        heightTable = (correctlyWagons.size() + 1) * rowHeight;

        contentStream.setStrokingColor(colorRGB[0], colorRGB[1], colorRGB[2]);
        // draw the rows
        float nexty = y - rowHeight;
        for (int i = 0; i <= rows - 1; i++) {
            contentStream.moveTo(margin, nexty);
            contentStream.lineTo(margin + tableWidth, nexty);
            contentStream.stroke();
            nexty -= rowHeight;
        }

        // now add the text
        contentStream.setFont(fontBold, 8);

        float textx = margin + cellMargin;
        float texty = y - 15;

        // HEADER TABLE
        for (int j = 0; j < cols; j++) {
            String text = headerTable[j];
            contentStream.beginText();
            contentStream.newLineAtOffset(textx, texty);
            contentStream.showText(text);
            contentStream.endText();
            textx += colWidth;
        }
        texty -= rowHeight;
        textx = margin + cellMargin;

        contentStream.setFont(fontNormal, 8);

        for (Map.Entry<Wagon, Long> entry : correctlyWagons.entrySet()) {
            Wagon k = entry.getKey();
            Long v = entry.getValue();

            for (int j = 0; j < cols; j++) {
                String text = "";
                switch (j) {
                case 0:
                    text = k.getIdentification();
                    break;
                case 1:
                    text = Integer.toString(k.getLoadCapacity());
                    break;
                case 2:
                    text = Long.toString(v);
                    break;
                case 3:
                    text = Integer.toString(k.getLease());
                    break;
                case 4:
                    text = Long.toString(v * k.getLease());
                    break;
                }

                contentStream.beginText();
                contentStream.newLineAtOffset(textx, texty);
                contentStream.showText(text);
                contentStream.endText();
                textx += colWidth;
            }
            texty -= rowHeight;
            textx = margin + cellMargin;
        }

    }

    private static Map<Wagon, Long> getWagonsCorrectly(List<Wagon> wagons) {

        List<Wagon> distinctWagons = wagons.stream()
                                           .filter(distinctByKey(w -> w.getId()))
                                           .collect(Collectors.toList());

        Map<Wagon, Long> wagonsMap = new HashMap<>();
        for (Wagon wagon : distinctWagons) {
            long sum = wagons.stream()
                             .filter(w -> wagon.getId() == w.getId())
                             .count();
            wagonsMap.put(wagon, sum);
        }
        return wagonsMap;

    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

}
