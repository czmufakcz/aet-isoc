package com.aetisoc.calculator;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.aetisoc.offer.domain.TRAIN_TYPE;
import com.aetisoc.offer.domain.Wagon;
import com.aetisoc.offer.model.WagonService;
import com.aetisoc.receivable.domain.COMMODITY_TYPE;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class Calculator {

    private final WagonService wagonService;

    public List<Wagon> getUsedWagons(COMMODITY_TYPE commodity, int quantity) throws Exception {
        Collection<Wagon> availableWagons = wagonService.findAllByUnitTypeByLoadCapacityDesc(commodity.getUnitType());

        List<Wagon> usedWagons = new LinkedList<>();
        int weightToAllocate = quantity;
        Wagon smallestWagonOfOurType = null;

        for (Wagon wagon : availableWagons) {
            int wagonsOfThisTypeUsed = weightToAllocate / wagon.getLoadCapacity();
            for (int i = 0; i < wagonsOfThisTypeUsed; i++)
                usedWagons.add(wagon);

            weightToAllocate -= wagonsOfThisTypeUsed * wagon.getLoadCapacity();
            smallestWagonOfOurType = wagon;
        }

        if (weightToAllocate > 0)
            usedWagons.add(smallestWagonOfOurType);

        return usedWagons;
    }

    public double calculatePrice(COMMODITY_TYPE commodity, int quantity, TRAIN_TYPE train, List<Wagon> wagons) {
        double commodityPrice = commodity.getPrice() * quantity;
        double trainPrice = train.getPrize();
        double wagonsPrice = wagons.stream()
                                   .mapToDouble(wagon -> wagon.getLease())
                                   .sum();

        return (double) Math.round((commodityPrice + trainPrice + wagonsPrice) * 100) / 100;
    }

}
