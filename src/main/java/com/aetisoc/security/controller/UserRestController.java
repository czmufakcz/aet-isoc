package com.aetisoc.security.controller;

import com.aetisoc.security.domain.Admin;
import com.aetisoc.security.domain.Businessman;
import com.aetisoc.security.domain.Customer;
import com.aetisoc.security.domain.Transiter;
import com.aetisoc.security.domain.User;
import com.aetisoc.security.model.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional(readOnly = true)
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/whoLogged")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getUserInfo(@AuthenticationPrincipal User user) throws Exception {
        switch (user.getRole()) {
            case ADMIN:
                return new ResponseEntity<Admin>((Admin) user, HttpStatus.OK);
            case BUSINESSMAN:
                return new ResponseEntity<Businessman>((Businessman) user, HttpStatus.OK);
            case CUSTOMER:
                return new ResponseEntity<Customer>((Customer) user, HttpStatus.OK);
            case TRANSITER:
                return new ResponseEntity<Transiter>((Transiter) user, HttpStatus.OK);
            default:
                return new ResponseEntity<String>("Unspecifc error.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/isEmailAlreadyExist")
    @Transactional(readOnly = true)
    public boolean getUserInfo(@RequestParam String email) throws Exception {
        if (email == null || email.isEmpty()) 
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email is not valid.");
        return userService.existsByEmail(email);
    }
}
