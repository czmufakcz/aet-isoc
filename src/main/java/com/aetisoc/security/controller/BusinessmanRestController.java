package com.aetisoc.security.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.aetisoc.security.domain.Businessman;
import com.aetisoc.security.model.BusinessmanService;
import com.aetisoc.security.model.UserService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/businessmans")
public class BusinessmanRestController {

    private final BusinessmanService businessmanService;
    private final UserService userService;

    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional
    public Businessman createBusinessman(@Valid @RequestBody Businessman businessman) throws Exception {
        if (businessman.getId() != null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID must be null.");
        if (userService.existsByEmail(businessman.getEmail()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Businessman with this email already exists.");
        
        return businessmanService.save(businessman);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional(readOnly = true)
    public Businessman getBusinessman(@PathVariable UUID id) {
        return businessmanService.findBusinessmanByID(id);
    }

}
