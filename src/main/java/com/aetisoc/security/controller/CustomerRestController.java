package com.aetisoc.security.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.aetisoc.security.domain.Customer;
import com.aetisoc.security.model.CustomerService;
import com.aetisoc.security.model.UserService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {

    private final CustomerService customerService;
    private final UserService userService;

    @PostMapping
    @Transactional
    public Customer createCustomer(@Valid @RequestBody Customer customer) throws Exception {
        if (customer.getId() != null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID must be null.");
        if (userService.existsByEmail(customer.getEmail()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Customer with this email already exists.");
        if (customer.isCompany() && (customer.getCin() == null || customer.getVatin() == null))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cin and vatin must be filled, when company is true.");
        if (!customer.isCompany() && (customer.getCin() != null || customer.getVatin() != null))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cin and vatin must be null, when company is false.");

        return customerService.save(customer);
    }

}
