package com.aetisoc.security.domain;

public interface IRole {
    ROLE getRole();
}
