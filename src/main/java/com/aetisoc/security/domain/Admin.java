package com.aetisoc.security.domain;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue(value = "ADMIN")
public class Admin extends User implements IRole {
    
    private static final long serialVersionUID = -6125371469991815027L;
    
    @Builder
    public Admin(UUID id, String firstname, String lastname, String password, String email, String phone, boolean locked, boolean active, LocalDateTime accountExpires, LocalDateTime passwordExpires) {
        super(id, firstname, lastname, password, email, phone, locked, active, accountExpires, passwordExpires);
    }
    
    @Override
    @Transient
    public ROLE getRole() {
        return ROLE.ADMIN;
    }

}
