package com.aetisoc.security.domain;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.aetisoc.receivable.domain.Receivable;
import com.aetisoc.security.model.Address;
import com.aetisoc.validators.ValidRange;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true, exclude = "receivable")
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue(value = "CUSTOMER")
public class Customer extends User implements IRole {

    private static final long serialVersionUID = -2307653204602722797L;

    @JsonIgnore
    private Set<Receivable> receivable;

    @NotNull
    private Address address;
    @NotNull
    private boolean company;
    @JsonInclude(Include.NON_NULL)
    @ValidRange(min = 8, max=8)
    private String cin; // ICO
    @JsonInclude(Include.NON_NULL)
    @ValidRange(min = 12, max=12)
    private String vatin; // DIC
   
    @Builder
    public Customer(UUID id, String firstname, String lastname, String password, String email, String phone, boolean company, String cin, String vatin, Address address, boolean locked, boolean active, LocalDateTime accountExpires, LocalDateTime passwordExpires) {
        super(id, firstname, lastname, password, email, phone, locked, active, accountExpires, passwordExpires);
        this.company = company;
        this.cin = cin;
        this.vatin = vatin;
        this.address = address;
    }

    @Override
    @Transient
    public ROLE getRole() {
        return ROLE.CUSTOMER;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "customer")
    public Set<Receivable> getReceivable() {
        return this.receivable;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    @Valid
    public Address getAddress() {
        return this.address;
    }

}
