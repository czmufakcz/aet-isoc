package com.aetisoc.security.domain;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue(value = "BUSINESSMAN")
public class Businessman extends User implements IRole{

    private static final long serialVersionUID = -4122063064319474398L;

    @NotEmpty
    private String job;
    
    @Builder
    public Businessman(UUID id, String firstname, String lastname, String password, String email, String phone, boolean locked, boolean active, LocalDateTime accountExpires, LocalDateTime passwordExpires, String job) {
        super(id, firstname, lastname, password, email, phone, locked, active, accountExpires, passwordExpires);
        this.job = job;
    }

    @Override
    @Transient
    public ROLE getRole() {
        return ROLE.BUSINESSMAN;
    }
    
}
