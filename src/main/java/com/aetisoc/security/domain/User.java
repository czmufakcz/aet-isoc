package com.aetisoc.security.domain;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "role", discriminatorType = DiscriminatorType.STRING)
@Table(name = "users")
public class User implements UserDetails, IRole {

    private static final long serialVersionUID = 6378470090764238211L;

    private UUID id;
    @NotEmpty
    private String firstname;
    @NotEmpty
    private String lastname;
    @JsonProperty(access = Access.WRITE_ONLY)
    @NotEmpty
    private String password;
    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    @Size(min = 9, max = 9)
    @Pattern(regexp = "(^$|[0-9]{9})")
    private String phone;
    @JsonIgnore
    private boolean locked = false;
    @JsonIgnore
    private boolean active = true;
    @JsonIgnore
    private LocalDateTime accountExpires;
    @JsonIgnore
    private LocalDateTime passwordExpires;

    @Id
    @GeneratedValue
    public UUID getId() {
        return this.id;
    }

    @Transient
    public ROLE getRole() {
        return null;
    }

    @Override
    @JsonIgnore
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authrotiy = new HashSet<>();
        authrotiy.add(new SimpleGrantedAuthority("ROLE_" + this.getRole()
                                                               .name()
                                                               .toUpperCase()));
        return authrotiy;
    }

    @JsonIgnore
    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return accountExpires == null || accountExpires.isAfter(LocalDateTime.now());
    }

    @JsonIgnore
    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return !isLocked();
    }

    @JsonIgnore
    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return accountExpires == null || passwordExpires.isAfter(LocalDateTime.now());
    }

    @JsonIgnore
    @Override
    @Transient
    public boolean isEnabled() {
        return isActive();
    }

    @JsonIgnore
    @Override
    @Transient
    public String getUsername() {
        return email;
    }
}
