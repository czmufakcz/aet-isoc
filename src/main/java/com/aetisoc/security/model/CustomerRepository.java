package com.aetisoc.security.model;

import javax.transaction.Transactional;

import com.aetisoc.security.domain.Customer;

@Transactional
public interface CustomerRepository extends UserBaseRepository<Customer>{

}

