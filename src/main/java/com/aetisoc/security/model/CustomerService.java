package com.aetisoc.security.model;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.aetisoc.security.domain.Customer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Customer save(Customer customer) {
        String encodePassword = passwordEncoder.encode(customer.getPassword());
        customer.setPassword(encodePassword);

        Customer savedBusinessman = customerRepository.save(customer);
        log.info("Successfully to saved customer to DB: {}", savedBusinessman);

        return savedBusinessman;
    }

    public Customer findCustomerByID(UUID id) {
        return customerRepository.findById(id)
                                 .orElseThrow(() -> new ResourceNotFoundException());
    }
    
    public void delete(Customer customer) {
    	customerRepository.delete(customer);
    }

}
