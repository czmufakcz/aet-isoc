package com.aetisoc.security.model;


import javax.transaction.Transactional;

import com.aetisoc.security.domain.User;

@Transactional
public interface UserRepository extends UserBaseRepository<User> {

    boolean existsByEmail(String email);
}
