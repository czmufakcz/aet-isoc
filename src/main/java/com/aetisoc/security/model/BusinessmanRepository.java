package com.aetisoc.security.model;

import javax.transaction.Transactional;

import com.aetisoc.security.domain.Businessman;

@Transactional
public interface BusinessmanRepository extends UserBaseRepository<Businessman>{

    
}
