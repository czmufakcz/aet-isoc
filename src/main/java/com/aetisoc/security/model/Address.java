package com.aetisoc.security.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "addresses")
public class Address implements Serializable{

    private static final long serialVersionUID = 4406821808252971701L;
    
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "addresses_id_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;
    @NotEmpty
    private String street;
    @NotEmpty
    private String descriptionNumber;
    @NotEmpty
    private String city;
    @NotEmpty
    @Size(min = 6, max = 6)
    @Pattern(regexp = "(^\\d{3}\\s\\d{2}$)")
    private String postcode;
    
}
