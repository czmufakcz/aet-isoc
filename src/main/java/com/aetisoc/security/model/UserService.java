package com.aetisoc.security.model;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.aetisoc.security.domain.User;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service(value = "UserDetailsService")
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
        
    public List<User> getUsers() {
        return userRepository.findAll();
    }
    
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email)
                             .orElseThrow(() -> new UsernameNotFoundException(MessageFormat.format("User with email {0} was not found.", email)));
    }
}
