package com.aetisoc.security.model;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.aetisoc.security.domain.Businessman;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class BusinessmanService {

    private final BusinessmanRepository businessmanRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Businessman save(Businessman businessman) {
        String encodePassword = passwordEncoder.encode(businessman.getPassword());
        businessman.setPassword(encodePassword);

        Businessman savedBusinessman = businessmanRepository.save(businessman);
        log.info("Successfully to saved businessman to DB: {}", savedBusinessman);
        
        return savedBusinessman;
    }

    public Businessman findBusinessmanByID(UUID id) {
        return businessmanRepository.findById(id)
                                    .orElseThrow(() -> new ResourceNotFoundException());
    }
    
    public void delete(Businessman businessman) {
    	businessmanRepository.delete(businessman);
    }
}
