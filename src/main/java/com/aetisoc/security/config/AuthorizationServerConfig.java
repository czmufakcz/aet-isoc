package com.aetisoc.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    private final static String CLIENT = "aet";
    private final static String SECRET = "isoc";
    private final static String SCOPE_READ = "read";
    private final static String SCOPE_WRITE = "write";
    private final static String GRANT_PASSWORD = "password";
    private final static String GRANT_CODE = "authorization_code";
    private final static String GRANT_REFRESH = "refresh_token";
    private final static String GRANT_IMPLICIT = "implicit";

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
               .withClient(CLIENT)
               .secret(passwordEncoder.encode(SECRET))
                .authorizedGrantTypes(GRANT_PASSWORD, GRANT_REFRESH, GRANT_CODE, GRANT_IMPLICIT)
               .scopes(SCOPE_READ, SCOPE_WRITE);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager)
                 .userDetailsService(userDetailsService)
                 .tokenStore(new JdbcTokenStore(dataSource));
    }

}
