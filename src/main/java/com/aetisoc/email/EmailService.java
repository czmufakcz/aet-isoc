package com.aetisoc.email;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.CharEncoding;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.aetisoc.security.domain.User;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailService {

    private static final String CUTOMER_RECEIVABLES_URL = "http://localhost";
    @Value("${mailer.enabled}")
    private Boolean enableMailer;
    private final JavaMailSender emailSender;

    public void sendOffersCreatedEmail(User targetUser) {
        try {
            MimeMessage mail = emailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mail);

            messageHelper.setTo(targetUser.getEmail());
            messageHelper.setSubject("ISOČ - Nabídka vytvořena :)");
            messageHelper.setText(createOfferCreatedEmailText(targetUser.getLastname(), CUTOMER_RECEIVABLES_URL), true);

            sendMail(mail);
        } catch (MessagingException e) {
            log.error("sendOffersCreatedEmail", e);
        }
    }

    public void sendDeliveredPackagedEmail(User targetUser, String toCity) {
        try {
            MimeMessage mail = emailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mail);

            messageHelper.setTo(targetUser.getEmail());
            messageHelper.setSubject("ISOČ - Nabídka vytvořena :)");
            messageHelper.setText(packageIsDeliveredEmailText(targetUser.getLastname(), toCity), true);

            sendMail(mail);
        } catch (MessagingException e) {
            log.error("sendOffersCreatedEmail", e);
        }
    }

    private String createOfferCreatedEmailText(String lastName, String link) {
        return new StringBuilder().append("<p align=\"left\"> Dobrý den pane/paní ")
                                  .append(lastName)
                                  .append(", </p>")
                                  .append("<p align=\"justify\"> Systém na jednu z Vašich pohledávek vygeneroval možná <a href=\"")
                                  .append(link)
                                  .append("\">řešení</a>. " + "Měl byste si ale pospíšit s potvrzením, " + "než Vaši trasu zarezervuje někdo jiný... </p>")
                                  .append("<p align=\"left\"> S pozdravem, <br> &#9;Váš <b>ISOČ</b> </p>")
                                  .toString();
    }

    private String packageIsDeliveredEmailText(String lastName, String toCity) {
        return new StringBuilder().append("<p align=\"left\"> Dobrý den pane/paní ")
                                  .append(lastName)
                                  .append(", </p>")
                                  .append("<p align=\\\"justify\\\"> Váše zboží dorazilo do konečné stanice " + toCity + ".</p>")
                                  .append("<p align=\"left\"> S pozdravem, <br> &#9;Váš <b>ISOČ</b> </p>")
                                  .toString();
    }

    public void sendNoOffersEmail(User targetUser) {
        try {
            MimeMessage mail = emailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mail);

            messageHelper.setTo(targetUser.getEmail());
            messageHelper.setSubject("ISOČ - Pohledávku nebylo možné splnit :(");
            messageHelper.setText(createOfferDicardedEmailText(targetUser.getLastname()), true);

            sendMail(mail);
        } catch (MessagingException e) {
            log.error("sendNoOffersEmail", e);
        }
    }

    private String createOfferDicardedEmailText(String lastName) {
        return new StringBuilder().append("<p align=\"left\"> Dobrý den pane/paní ")
                                  .append(lastName)
                                  .append(", </p>")
                                  .append("<p align=\"justify\"> Systém na jednu z Vašich pohledávek bohužel " + "nenalezl žádná řešení, " + "protože všechny možné trasy jsou již obsazené.  </p>" + "<p align=\"justify\"> Protože vytížení provozu bedlivě sledujeme, " + "kapacita Vaší trasy bude jistě brzy navýšena. </p>")
                                  .append("<p align=\"left\"> S pozdravem, <br> &#9;Váš <b>ISOČ</b> </p>")
                                  .toString();
    }

    public void sendECLEmail(User targetUser, File attachmentECL) {
        try {
            MimeMessage mail = emailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mail, true, CharEncoding.UTF_8);

            messageHelper.setTo(targetUser.getEmail());
            messageHelper.setSubject("ISOČ - Elektronický nákladní list");
            messageHelper.setText(createECLMailText(targetUser.getLastname()), true);
            messageHelper.addAttachment("ECL", attachmentECL);

            sendMail(mail);
        } catch (MessagingException e) {
            log.error("sendECLEmail", e);
        }
    }

    private String createECLMailText(String lastName) {
        return new StringBuilder().append("<p align=\"left\"> Dobrý den pane/paní ")
                                  .append(lastName)
                                  .append(", </p>")
                                  .append("<p align=\"justify\"> V příloze Vám posíláme elektronický nákladní list " + "pro jednu z Vašich pohledávek. </p>")
                                  .append("<p align=\"left\"> S pozdravem, <br> &#9;Váš <b>ISOČ</b> </p>")
                                  .toString();
    }

    private void sendMail(MimeMessage mail) {
        if (enableMailer)
            emailSender.send(mail);
    }

}