package com.aetisoc.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RangeValidator implements ConstraintValidator<ValidRange, String> {

    private int min;
    private int max;

    public void initialize(ValidRange validRangeAnnotation) {
        this.min = validRangeAnnotation.min();
        this.max = validRangeAnnotation.max();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null)
            return true;
        return !(value.length() < min || value.length() > max);
    }

}
