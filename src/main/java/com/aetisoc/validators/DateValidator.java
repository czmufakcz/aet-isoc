package com.aetisoc.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.aetisoc.receivable.domain.IDeliveryRange;

public class DateValidator implements ConstraintValidator<ValidDate, IDeliveryRange> {

    @Override
    public boolean isValid(IDeliveryRange deliveryRange, ConstraintValidatorContext context) {
        if (deliveryRange == null)
            return true;
        if(deliveryRange.getDeliveryDate() == null || deliveryRange.getDepartureDate() == null)
            return false;
        return deliveryRange.getDeliveryDate().isAfter(deliveryRange.getDepartureDate());
    }

}
