package com.aetisoc.statistics;

import java.time.LocalDateTime;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aetisoc.offer.domain.OFFER_STATE;
import com.aetisoc.offer.model.OfferService;
import com.aetisoc.receivable.domain.RECEIVABLE_STATE;
import com.aetisoc.receivable.model.ReceivableService;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/statistics")
public class StatisticsRestController {

    private final OfferService offerService;
    private final ReceivableService receivableService;

    @GetMapping("/lastMonth")
    @PreAuthorize("hasRole('ROLE_BUSINESSMAN')")
    @Transactional(readOnly = true)
    public Statistics getOffersCreated() throws Exception {

        LocalDateTime initial = LocalDateTime.now();
        LocalDateTime startDate = initial.withDayOfMonth(1);
        LocalDateTime endDate = initial.withDayOfMonth(initial.getDayOfMonth());

        int offerCreated = offerService.getOfferBetween(startDate, endDate, OFFER_STATE.CREATED)
                                       .size();
        int offerAccepted = offerService.getOfferBetween(startDate, endDate, OFFER_STATE.ACCEPTED_BY_CUSTOMER)
                .size();
        int offerCanceled = offerService.getOfferBetween(startDate, endDate, OFFER_STATE.CANCELLED)
                .size();
        int offerDelivered = offerService.getOfferBetween(startDate, endDate, OFFER_STATE.DELIVERED)
                .size();
        
        int receivableCreated = receivableService.getReceivableBetween(startDate, endDate, RECEIVABLE_STATE.CREATED).size();
        int receivableCanceled = receivableService.getReceivableBetween(startDate, endDate, RECEIVABLE_STATE.CANCELED_BY_CUSTOMER).size();
        int receivableDone = receivableService.getReceivableBetween(startDate, endDate, RECEIVABLE_STATE.ACCEPTED_BY_CUSTOMER).size();

        return Statistics.builder()
                         .createdOffer(offerCreated)
                         .acceptedOffer(offerAccepted)
                         .deliveredOffer(offerDelivered)
                         .canceledOffer(offerCanceled)
                         .createdReceivable(receivableCreated)
                         .canceledReceivable(receivableCanceled)
                         .doneReceivable(receivableDone)
                         .build();
    }

    @Data
    @Builder
    private static class Statistics {
        private int createdOffer;
        private int acceptedOffer;
        private int canceledOffer;
        private int deliveredOffer;

        private int createdReceivable;
        private int canceledReceivable;
        private int doneReceivable;

    }
}
