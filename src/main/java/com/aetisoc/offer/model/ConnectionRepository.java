package com.aetisoc.offer.model;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.aetisoc.offer.domain.Connection;

public interface ConnectionRepository extends JpaRepository<Connection, Long> {

    @Query(value = "SELECT DISTINCT from_city FROM connections ORDER BY from_city ASC", nativeQuery = true)
    Collection<String> findDistinctFromCity();
    
    @Query(value = "SELECT DISTINCT to_city FROM connections ORDER BY to_city ASC", nativeQuery = true)
    Collection<String> findDistinctToCity();
    
    boolean existsByFromCity(String fromCity);
    
    boolean existsByToCity(String toCity);
}
