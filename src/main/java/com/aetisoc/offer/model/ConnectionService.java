package com.aetisoc.offer.model;

import java.util.Collection;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.aetisoc.offer.domain.Connection;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class ConnectionService {

    private final ConnectionRepository connectionRepository;

    public Connection save(Connection connection) {
        Connection savedConnection = connectionRepository.save(connection);
        log.info("Successfully to saved connection to DB: {}", savedConnection);
        return savedConnection;
    }

    public Collection<Connection> findAllByFromCityAndToCity(String from, String to) {
        Connection example = Connection.builder()
                                       .fromCity(from)
                                       .toCity(to)
                                       .build();

        return connectionRepository.findAll(Example.of(example));
    }

    public Collection<Connection> findAllByFromCity(String from) {
        Connection example = Connection.builder()
                                       .fromCity(from)
                                       .build();

        return connectionRepository.findAll(Example.of(example));
    }

    public Collection<Connection> findAll() {
        return connectionRepository.findAll();
    }

    public void delete(Long id) {
        connectionRepository.deleteById(id);
    }

    public Collection<String> findDistinctFromCity() {
        return connectionRepository.findDistinctFromCity();
    }

    public Collection<String> findDistinctToCity() {
        return connectionRepository.findDistinctToCity();
    }

    public boolean isExistFromCity(String fromCity) {
        return connectionRepository.existsByFromCity(fromCity);
    }
    
    public boolean isExistToCity(String toCity) {
        return connectionRepository.existsByToCity(toCity);
    }
    
}