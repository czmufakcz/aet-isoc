package com.aetisoc.offer.model;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.aetisoc.offer.domain.Connection;
import com.aetisoc.offer.domain.OFFER_STATE;
import com.aetisoc.offer.domain.Offer;
import com.aetisoc.path.generator.Path;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class OfferService {

    private final OfferRepository offerRepository;

    public Offer save(Offer offer) {
        Offer savedOffer = offerRepository.save(offer);
        log.info("Successfully to saved offer to DB: {}", savedOffer);
        return savedOffer;
    }

    public Optional<Offer> findByIdOffer(Long id) {
        return offerRepository.findById(id);
    }

    public Collection<Offer> findAllByOfferState(OFFER_STATE offerState) {
        return offerRepository.findAllByOfferState(offerState);
    }

    public Collection<Offer> findAllByOfferStateAndCustomerId(UUID customerID, OFFER_STATE offerState) {
        return offerRepository.findAllByOfferState(offerState)
                              .stream()
                              .filter(offer -> offer.getOriginal()
                                                    .getCustomer()
                                                    .getId()
                                                    .equals(customerID))
                              .collect(Collectors.toList());
    }

    public Optional<Offer> findByReceivableId(Long id) {
        return offerRepository.findByReceivableId(id);
    }

    public boolean isConnectionAvailable(Connection connection, LocalDateTime departure, LocalDateTime delivery, int requiredBandwidth) {
        Collection<Offer> offers = offerRepository.findAllByOfferState(OFFER_STATE.ACCEPTED_BY_CUSTOMER);

        int takenBandwidth = 0;

        for (Offer offer : offers) {
            if (departure.isBefore(offer.getDeliveryDate()) && delivery.isAfter(offer.getDepartureDate())) {
                for (Path path : offer.getPaths()) {
                    if (path.hasReservedConnection(connection))
                        takenBandwidth++;
                }
            }
        }

        return connection.getBandwidth() - takenBandwidth >= requiredBandwidth;
    }

    public void delete(Offer offer) {
        offerRepository.delete(offer);
    }

    public Collection<Offer> getOfferBetween(LocalDateTime startDate, LocalDateTime endDate, OFFER_STATE offerState) {
        return offerRepository.findAllByCreatedAtBetweenAndOfferState(startDate, endDate, offerState);
    }

}
