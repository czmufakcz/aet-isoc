package com.aetisoc.offer.model;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aetisoc.offer.domain.UNIT_TYPE;
import com.aetisoc.offer.domain.Wagon;

@Repository
public interface WagonRepository extends JpaRepository<Wagon, Long> {

    Collection<Wagon> findAllByUnitTypeOrderByLoadCapacityDesc(UNIT_TYPE unitType);
}
