package com.aetisoc.offer.model;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.aetisoc.offer.domain.OFFER_STATE;
import com.aetisoc.offer.domain.Offer;

public interface OfferRepository extends JpaRepository<Offer, Long> {

    Collection<Offer> findAllByOfferState(OFFER_STATE offerState);
    
    @Query(value = " SELECT * FROM offers WHERE receivable_id = :id", nativeQuery = true)
    Optional<Offer> findByReceivableId(@Param("id") Long id);

    Collection<Offer> findAllByCreatedAtBetweenAndOfferState(LocalDateTime startStart, LocalDateTime endDate, OFFER_STATE offerState);
    
}
