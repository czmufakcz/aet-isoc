package com.aetisoc.offer.model;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.aetisoc.offer.domain.UNIT_TYPE;
import com.aetisoc.offer.domain.Wagon;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class WagonService {
    
    private final WagonRepository wagonRepository;
    
    public Collection<Wagon> findAllByUnitTypeByLoadCapacityDesc(UNIT_TYPE unitType){
        return wagonRepository.findAllByUnitTypeOrderByLoadCapacityDesc(unitType);
    }
}
