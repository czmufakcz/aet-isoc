package com.aetisoc.offer.domain;

import com.aetisoc.receivable.domain.IState;
import com.aetisoc.receivable.domain.StateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
@JsonSerialize(using = StateSerializer.class)
public enum OFFER_STATE implements IState {
    CREATED("vytvořena"), ACCEPTED_BY_CUSTOMER("schváleno zákazníkem"), CANCELLED("zrušeno"), DELIVERED("doručeno");
    
    private final String translate;
}
