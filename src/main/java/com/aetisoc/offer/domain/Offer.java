package com.aetisoc.offer.domain;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.annotations.CreationTimestamp;

import com.aetisoc.path.generator.Path;
import com.aetisoc.receivable.domain.IDeliveryRange;
import com.aetisoc.receivable.domain.Receivable;
import com.aetisoc.security.domain.Businessman;
import com.aetisoc.validators.ValidDate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
@EqualsAndHashCode
@ValidDate
@Entity
@Table(name = "offers")
public class Offer implements IDeliveryRange {

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "offers_id_seq", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "receivable_id", nullable = false)
    private Receivable original;
    @ManyToOne
    @JoinColumn(name = "businessman_id", nullable = false)
    private Businessman businessman;
    private LocalDateTime departureDate;
    private LocalDateTime deliveryDate;
    @CreationTimestamp
    private LocalDateTime createdAt;
    @Positive
    private double priceResource;
    @Enumerated(EnumType.STRING)
    private OFFER_STATE offerState;
    @OneToMany(mappedBy = "offer", cascade = CascadeType.ALL)
    private List<Path> paths;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "offer_id")
    private List<TrackingStation> trackingStation;
    private int sale;
    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "resource_id")
    private Resources resources;

    @Transient
    public Path getActivePath() {
        return paths.stream()
                    .filter(path -> path.isAccepted())
                    .findFirst()
                    .orElse(null);
    }

    public double getFinalPrice() {
        if (offerState != OFFER_STATE.ACCEPTED_BY_CUSTOMER)
            return 0;
        double suma = getActivePath().getPriceByConnections() + priceResource;
        double salePrice = ((double) sale / 100) * suma;
        return suma - salePrice;
    }
}
