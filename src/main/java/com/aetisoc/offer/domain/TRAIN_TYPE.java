package com.aetisoc.offer.domain;

public enum TRAIN_TYPE {
    T10, T15, T20, T25;

    public double getPrize() {
        switch (this) {
        case T10:
            return 7000;
        case T15:
            return 8000;
        case T20:
            return 9000;
        case T25:
            return 10000;
        default:
            throw new IllegalArgumentException("Wrong TRAIN_TYPE.");
        }
    }

    public static TRAIN_TYPE getTrain(int countWagons) {
        if (countWagons < 0)
            throw new IllegalArgumentException("Invalid countWargons.");

        if (10 >= countWagons)
            return T10;
        else if (10 < countWagons && 15 >= countWagons)
            return T15;
        else if (15 < countWagons && 20 >= countWagons)
            return T20;
        else
            return T25;
    }

}
