package com.aetisoc.offer.controller;

import com.aetisoc.calculator.Calculator;
import com.aetisoc.calculator.PDFGenerator;
import com.aetisoc.email.EmailService;
import com.aetisoc.offer.domain.OFFER_STATE;
import com.aetisoc.offer.domain.Offer;
import com.aetisoc.offer.domain.Resources;
import com.aetisoc.offer.domain.TRAIN_TYPE;
import com.aetisoc.offer.domain.TrackingStation;
import com.aetisoc.offer.domain.Wagon;
import com.aetisoc.offer.model.OfferService;
import com.aetisoc.path.generator.Path;
import com.aetisoc.path.generator.PathService;
import com.aetisoc.receivable.domain.RECEIVABLE_STATE;
import com.aetisoc.receivable.domain.Receivable;
import com.aetisoc.receivable.model.ReceivableService;
import com.aetisoc.security.domain.Businessman;
import com.aetisoc.security.domain.ROLE;
import com.aetisoc.security.domain.User;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/offers")
public class OfferRestController {

    private final OfferService offerService;
    private final ReceivableService receivableService;
    private final EmailService emailService;
    private final PathService pathService;
    private final Calculator calculator;

    @PostMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_BUSINESSMAN')")
    @Transactional
    public Offer createOffer(@PathVariable Long id, @Valid @RequestBody Offer offer, @AuthenticationPrincipal Businessman businessman) throws Exception {
        Receivable originalReceivable = receivableService.findByIdReceivable(id)
                                                         .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Receivable not found."));
        if (offer.getPaths()
                 .isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Paths is empty.");

        if (originalReceivable.getDepartureDate()
                              .isBefore(LocalDateTime.now()
                                                     .minusDays(14)))
            new ResponseStatusException(HttpStatus.BAD_REQUEST, "It's too late.");

        offer.setOriginal(originalReceivable);
        offer.setDeliveryDate(originalReceivable.getDeliveryDate());
        offer.setDepartureDate(originalReceivable.getDepartureDate());
        offer.setBusinessman(businessman);
        offer.setOfferState(OFFER_STATE.CREATED);
        originalReceivable.setReceivableState(RECEIVABLE_STATE.PROCESS);

        emailService.sendOffersCreatedEmail(originalReceivable.getCustomer());

        offer.getPaths()
             .stream()
             .forEach(path -> path.setOffer(offer));

        return offerService.save(offer);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_CUSTOMER') OR hasRole('ROLE_BUSINESSMAN')")
    @Transactional(readOnly = true)
    public Offer getOfferByIdReceivable(@PathVariable Long id) throws Exception {
        return offerService.findByReceivableId(id)
                           .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Receivable not found."));

    }

    @GetMapping("/state/{state}")
    @PreAuthorize("hasRole('ROLE_CUSTOMER') OR hasRole('ROLE_BUSINESSMAN') OR hasRole('ROLE_TRANSITER')")
    @Transactional(readOnly = true)
    public Collection<Offer> getOfferByState(@PathVariable OFFER_STATE state, @AuthenticationPrincipal User user) throws Exception {
        if (user.getRole() == ROLE.CUSTOMER) {
            if (state == OFFER_STATE.ACCEPTED_BY_CUSTOMER) {
                Collection<Offer> offers = offerService.findAllByOfferStateAndCustomerId(user.getId(), state);
                offers.addAll(offerService.findAllByOfferStateAndCustomerId(user.getId(), OFFER_STATE.DELIVERED));
                return offers;
            }
            return offerService.findAllByOfferStateAndCustomerId(user.getId(), state);
        } else {
            if (state == OFFER_STATE.ACCEPTED_BY_CUSTOMER) {
                Collection<Offer> offers = offerService.findAllByOfferState(state);
                offers.addAll(offerService.findAllByOfferState(OFFER_STATE.DELIVERED));
                return offers;
            }
            return offerService.findAllByOfferState(state);
        }

    }

    @PostMapping("/{id}/changeCity")
    @PreAuthorize("hasRole('ROLE_TRANSITER')")
    @Transactional
    public Offer changeActualStation(@PathVariable Long id, @RequestBody String actualCity) throws Exception {
        Offer offer = offerService.findByIdOffer(id)
                                  .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Offer not found."));

        if (offer.getOfferState() != OFFER_STATE.ACCEPTED_BY_CUSTOMER)
            new ResponseStatusException(HttpStatus.BAD_REQUEST, "Offer state must be in ACCEPTED_BY_CUSTOMER.");

        TrackingStation trackingStation = offer.getTrackingStation()
                                               .stream()
                                               .filter(track -> track.getCity()
                                                                     .equals(actualCity))
                                               .findFirst()
                                               .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid city. This city is not in path."));
        if (trackingStation.isCanSelect()) {
            trackingStation.setChanged(null);
            trackingStation.setCanSelect(false);
        } else {
            trackingStation.setChanged(LocalDateTime.now());
            trackingStation.setCanSelect(true);
        }

        if (offer.getTrackingStation()
                 .get(offer.getTrackingStation()
                           .size() - 1)
                 .isCanSelect()) {
            offer.setOfferState(OFFER_STATE.DELIVERED);
            offer.getOriginal()
                 .setReceivableState(RECEIVABLE_STATE.DELIVERED);
            emailService.sendDeliveredPackagedEmail(offer.getOriginal()
                                                         .getCustomer(), offer.getOriginal()
                                                                              .getToCity());

        } else {
            offer.setOfferState(OFFER_STATE.ACCEPTED_BY_CUSTOMER);
            offer.getOriginal()
                 .setReceivableState(RECEIVABLE_STATE.ACCEPTED_BY_CUSTOMER);
        }

        return offerService.save(offer);
    }

    @GetMapping("/{id}/getAllCitiesOnPath")
    @PreAuthorize("hasRole('ROLE_TRANSITER')")
    @Transactional
    public List<String> getAllCitiesOnPath(@PathVariable Long id) throws Exception {
        Offer offer = offerService.findByIdOffer(id)
                                  .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Offer not found."));

        if (offer.getOfferState() != OFFER_STATE.ACCEPTED_BY_CUSTOMER)
            new ResponseStatusException(HttpStatus.BAD_REQUEST, "Offer state must be in ACCEPTED_BY_CUSTOMER.");

        return offer.getActivePath()
                    .getAllCitiesBeetwenFromToCity();
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_BUSINESSMAN')")
    @Transactional
    public Offer updateOffer(@Valid @RequestBody Offer offer, @AuthenticationPrincipal Businessman businessman) throws Exception {
        Offer originalOffer = getOfferById(offer.getId());
        if (originalOffer.getOfferState() != OFFER_STATE.CREATED)
            new ResponseStatusException(HttpStatus.BAD_REQUEST, "Offer must be in state CREATED.");

        originalOffer.setDepartureDate(offer.getDepartureDate());
        originalOffer.setDeliveryDate(offer.getDeliveryDate());
        originalOffer.setResources(offer.getResources());
        originalOffer.setPaths(offer.getPaths());
        originalOffer.setPriceResource(offer.getPriceResource());
        return offerService.save(offer);
    }

    @GetMapping("/generate-offer/{idReceivable}")
    @PreAuthorize("hasRole('ROLE_BUSINESSMAN')")
    @Transactional
    public Offer generateOffer(@PathVariable Long idReceivable, @AuthenticationPrincipal Businessman businessman) throws Exception {
        Receivable originalReceivable = receivableService.findByIdReceivable(idReceivable)
                                                         .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Receivable not found."));

        List<Wagon> usedWagons = calculator.getUsedWagons(originalReceivable.getCommodityType(), originalReceivable.getQuantity());
        TRAIN_TYPE train = TRAIN_TYPE.getTrain(usedWagons.size());

        double priceResources = calculator.calculatePrice(originalReceivable.getCommodityType(), originalReceivable.getQuantity(), train, usedWagons);

        Resources resources = Resources.builder()
                                       .train(train)
                                       .wagons(usedWagons)
                                       .build();

        List<Path> paths = pathService.generatePaths(originalReceivable.getFromCity(), originalReceivable.getToCity(), 1, originalReceivable.getDepartureDate(), originalReceivable.getDeliveryDate());
        Offer offer = Offer.builder()
                           .priceResource(priceResources)
                           .paths(paths)
                           .resources(resources)
                           .build();

        paths.stream()
             .forEach(path -> path.setOffer(offer));

        return offer;
    }

    @PostMapping("/deccission/{id}")
    @PreAuthorize("hasRole('ROLE_CUSTOMER') OR hasRole('ROLE_BUSINESSMAN')")
    @Transactional
    public Offer decissiontOffer(@PathVariable Long id, @RequestBody OfferChoice offerChoice, @AuthenticationPrincipal User user) {
        if (offerChoice.getAccepted() == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid accepted value.");

        Offer originalOffer = getOfferById(id);
        checkAccessToOffer(originalOffer, user);

        if (user.getRole() == ROLE.CUSTOMER && originalOffer.getOfferState() != OFFER_STATE.CREATED)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Offer must be in state CREATED.");

        if (originalOffer.getDepartureDate()
                         .isBefore(LocalDateTime.now()
                                                .minusDays(14)))
            new ResponseStatusException(HttpStatus.BAD_REQUEST, "It's too late.");

        if (offerChoice.getAccepted()) {
            originalOffer.setOfferState(OFFER_STATE.ACCEPTED_BY_CUSTOMER);
            originalOffer.getOriginal()
                         .setReceivableState(RECEIVABLE_STATE.ACCEPTED_BY_CUSTOMER);

            Path reservationPath = originalOffer.getPaths()
                                                .stream()
                                                .filter(path -> path.getId()
                                                                    .equals(offerChoice.getPathID()))
                                                .findFirst()
                                                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Path is not found."));

            reservationPath.setAccepted(true);

            reservationPath.getAllCitiesBeetwenFromToCity()
                           .stream()
                           .forEach(city -> originalOffer.getTrackingStation()
                                                         .add(TrackingStation.builder()
                                                                             .city(city)
                                                                             .changed(null)
                                                                             .canSelect(false)
                                                                             .build()));

            emailService.sendECLEmail(originalOffer.getOriginal()
                                                   .getCustomer(), PDFGenerator.generatePDF(originalOffer));
        } else {
            originalOffer.setOfferState(OFFER_STATE.CANCELLED);
            originalOffer.getOriginal()
                         .setReceivableState(RECEIVABLE_STATE.CANCELED_BY_CUSTOMER);
        }

        return offerService.save(originalOffer);
    }

    private void checkAccessToOffer(Offer offer, User user) {
        if (user.getRole() != ROLE.BUSINESSMAN && !offer.getOriginal()
                                                        .getCustomer()
                                                        .getId()
                                                        .equals(user.getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This is not your offer.");
    }

    private Offer getOfferById(Long id) {
        return offerService.findByIdOffer(id)
                           .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Offer not found."));
    }

    @Data
    public static class OfferChoice {
        @NotNull
        private Boolean accepted;
        @NotNull
        private Long pathID;
    }
}
