package com.aetisoc.path.generator;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import com.aetisoc.offer.model.ConnectionService;
import com.aetisoc.offer.model.OfferService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PathService {

    private final OfferService offerService;
    private final ConnectionService connectionService;

    public List<Path> generatePaths(String fromCity, String toCity, int requiredBandwidth, LocalDateTime departure, LocalDateTime delivery) {
        PathGenerator generator = PathGenerator.builder()
                                               .offerService(offerService)
                                               .connectionService(connectionService)
                                               .requiredBandwidth(requiredBandwidth)
                                               .fromCity(fromCity)
                                               .toCity(toCity)
                                               .departure(departure)
                                               .delivery(delivery)
                                               .build();
        generator.generatePaths();
        return generator.getFoundPaths();
    }

}
