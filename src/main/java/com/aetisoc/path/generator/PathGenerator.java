package com.aetisoc.path.generator;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import com.aetisoc.offer.domain.Connection;
import com.aetisoc.offer.model.ConnectionService;
import com.aetisoc.offer.model.OfferService;

import lombok.Builder;
import lombok.Getter;

/**
 * Modified A* alghoritm for finding not just best path, but also some other in
 * reasonable range(MAX_ITERATIONS). Please note that it is working with
 * oriented graph.
 * 
 * @author mytrin
 */
@Builder
public class PathGenerator {

    public static final int MAX_ITERATIONS = 100;
    public static final int MAX_VARIANTS = 4;
    private int iterations;

    private OfferService offerService;
    private ConnectionService connectionService;

    private String fromCity;
    private String toCity;
    private LocalDateTime departure;
    private LocalDateTime delivery;

    private int requiredBandwidth;

    private final HashMap<String, Node> found = new HashMap<>();
    private final PriorityQueue<Node> toExplore = new PriorityQueue<>();

    @Getter
    private final List<Path> foundPaths = new LinkedList<>();

    public void generatePaths() {
        Node start = new Node(fromCity, true);
        found.put(fromCity, start);
        toExplore.add(start);

        while (!toExplore.isEmpty() && iterations < MAX_ITERATIONS && foundPaths.size() < MAX_VARIANTS) {
            Node explored = toExplore.poll();
            exploreNode(explored);
            iterations++;
        }
    }

    private void exploreNode(Node node) {
        for (Connection connection : connectionService.findAllByFromCity(node.name)) {
            if (offerService.isConnectionAvailable(connection, departure, delivery, requiredBandwidth)) {
                createOrUpdateNode(node, connection);
            }
        }
    }

    private void createOrUpdateNode(Node parent, Connection connection) {
        Node target = found.get(connection.getToCity());

        if (target == null && connection.getToCity()
                                        .equals(toCity)) {
            gatherPossiblePath(parent, connection);
        } else if (target == null) {
            target = new Node(connection.getToCity());
            found.put(target.name, target);

            target.addParentIfConnectionIsBetter(parent, connection);
            toExplore.add(target);
        } else {
            target.addParentIfConnectionIsBetter(parent, connection);
            updateQueuePriority(target);
        }
    }

    private void gatherPossiblePath(Node parent, Connection connection) {
        Node finish = new Node(connection.getToCity());
        finish.addParentIfConnectionIsBetter(parent, connection);
        foundPaths.add(finish.collectConnections());
    }

    private void updateQueuePriority(Node target) {
        if (toExplore.contains(target)) {
            toExplore.remove(target);
            toExplore.add(target);
        }
    }

    private class Node implements Comparable<Node> {

        private String name;
        private Node parent;
        private Connection connection;
        private boolean isStart;

        public Node(String name) {
            this(name, false);
        }

        public Node(String name, boolean isStart) {
            this.name = name;
            this.isStart = isStart;
        }

        private void addParentIfConnectionIsBetter(Node parent, Connection connection) {
            if (isConnectionBetter(parent, connection)) {
                this.parent = parent;
                this.connection = connection;
            }
        }

        private boolean isConnectionBetter(Node parent, Connection connection) {
            if (isStart)
                return false; // do not return to starting state.

            int costFromParent = parent.getTotalCost() + connection.getDuration();
            return this.connection == null || getTotalCost() > costFromParent;
        }

        @Override
        public int compareTo(Node o) {
            return getTotalCost() - o.getTotalCost();
        }

        private int getTotalCost() {
            if (parent == null)
                return 0;

            return parent.getTotalCost() + connection.getDuration();
        }

        private Path collectConnections() {
            List<Connection> connections = new LinkedList<>();
            collectConnections(connections);

            return Path.builder()
                       .connections(connections)
                       .build();
        }

        private void collectConnections(List<Connection> connections) {
            if (connection != null) {
                connections.add(0, connection);
            }

            if (parent != null) {
                parent.collectConnections(connections);
            }
        }

    }

}
