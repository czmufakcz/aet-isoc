package com.aetisoc.path.generator;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aetisoc.offer.domain.Connection;
import com.aetisoc.offer.domain.Offer;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@Entity
@Table(name = "paths")
public class Path {

    private static final int PRICE_PER_MIN = 30;

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "paths_id_seq", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;

    @Default
    private boolean accepted = false;

    @ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(name = "paths_connections", joinColumns = @JoinColumn(name = "paths_id"), inverseJoinColumns = @JoinColumn(name = "connections_id"))
    private List<Connection> connections;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "offer_id")
    private Offer offer;

    public String getFromCity() {
        return connections.get(0)
                          .getFromCity();
    }

    public String getToCity() {
        return connections.get(connections.size() - 1)
                          .getToCity();
    }

    public int getTimeLength() {
        return connections.stream()
                          .map((Connection connection) -> connection.getDuration())
                          .mapToInt(Integer::intValue)
                          .sum();
    }

    @Transient
    public double getPriceByConnections() {
        return PRICE_PER_MIN * getTimeLength();
    }

    @Transient
    @JsonGetter
    public double finalPrice() {
        double suma = getPriceByConnections() + offer.getPriceResource();
        double salePrice = ((double) offer.getSale() / 100) * suma;
        return suma - salePrice;
    }

    public boolean hasReservedConnection(Connection connection) {
        return accepted && connections.contains(connection);
    }

    public List<String> getAllCitiesBeetwenFromToCity() {
        List<String> cities = connections.stream()
                                         .map(con -> con.getFromCity())
                                         .collect(Collectors.toList());
        cities.add(getToCity());
        return cities;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Connection conn : connections) {
            sb.append(conn.getFromCity())
              .append(" -> ")
              .append(conn.getToCity())
              .append("; ");
        }

        return sb.toString();
    }

}
