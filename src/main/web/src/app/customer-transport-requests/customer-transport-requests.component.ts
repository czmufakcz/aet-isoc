import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
  MatPaginator,
  MatSnackBar,
  MatTableDataSource
} from "@angular/material";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {Offer, Receivable} from "../create-request/create-request.component";
import {Roles, UserService} from "../user-service/user.service";
import {User} from "../login/login.component";

@Component({
  selector: 'app-customer-transport-requests',
  templateUrl: './customer-transport-requests.component.html',
  styleUrls: ['./customer-transport-requests.component.css']
})
export class CustomerTransportRequestsComponent implements OnInit {

  private user: User;

  public requestsDatasource: MatTableDataSource<Receivable> = new MatTableDataSource();

  displayedColumns: string[] = ['fromCity', 'toCity', 'departureDate', 'deliveryDate', 'commodityType', 'quantity', 'receivableState', 'detail'];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private http: HttpClient, private snackBar: MatSnackBar, private router: Router, private userService: UserService, public dialog: MatDialog) {
  }

  ngOnDestroy() {
    this.user = null;
    this.requestsDatasource = new MatTableDataSource();
  }

  ngOnInit() {
    this.requestsDatasource.paginator = this.paginator;
    this.loadRequests();
  }

  getReceivablesForCustomer(): Observable<Receivable[]> {
    return this.http.get<Receivable[]>('/api/receivables');
  }

  getReceivablesForBusinessMan(): Observable<Receivable[]> {
    return this.http.get<Receivable[]>('/api/receivables/all');
  }

  async loadRequests() {
    this.user = await this.userService.getUserInfo();

    if (this.user) {
      if (this.user.role === Roles.CUSTOMER) {
        this.getReceivablesForCustomer().subscribe(data => {
            this.requestsDatasource.data = data;
          },
          error => {
          })
      }

      if (this.user.role === Roles.BUSINESSMAN) {
        this.getReceivablesForBusinessMan().subscribe(data => {
            this.requestsDatasource.data = data;
          },
          error => {
          })
      }
    }
  }

  openDialog(request) {

    let dialogRef;

    if (this.user.role === Roles.BUSINESSMAN) {
      dialogRef = this.dialog.open(ReceivableDialogBusinessMan, {
        width: '700px',
        data: {request: request},
      });

    }

    if (this.user.role === Roles.CUSTOMER) {
      dialogRef = this.dialog.open(ReceivableDialogCustomer, {
        width: '700px',
        data: {request: request},
      });

    }

    dialogRef.afterClosed().subscribe(result => {
      this.loadRequests();
    });
  }

  openStationsDialog(request) {
    let dialogRef;


    dialogRef = this.dialog.open(StationsDialog, {
      width: '700px',
      data: {request: request},
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  getOffers() {
    return this.http.get<Receivable[]>('/api/receivables/all')
  }

}

@Component({
  selector: 'receivable-dialog-businessman',
  templateUrl: 'dialogs/receivable-dialog-businessman.html',
  styleUrls: ['./customer-transport-requests.component.css']
})
export class ReceivableDialogBusinessMan {

  offersGenerated: boolean;

  offer: Offer;

  constructor(
    public dialogRef: MatDialogRef<ReceivableDialogBusinessMan>, private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private snackBar: MatSnackBar) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

    let url = `/api/offers/${this.data.request.id}`;

    this.http.get<Offer>(url,).subscribe(data => {
        this.offer = data;
        this.offersGenerated = true;
      },
      error => {
        this.offersGenerated = false;
      })
  }


  generateOffer(offerId) {

    let url = `/api/offers/generate-offer/${offerId}`;
    this.http.get<Offer>(url).subscribe(data => {
      this.offer = data;
      this.offersGenerated = true;
      this.showNotification('Nabídka vygenerována!', 'green-snackbar');
    }, error => {
    });
  }


  sendOffer(request: Receivable) {
    let url = `/api/offers/${request.id}`;

    this.offer.deliveryDate = request.deliveryDate;
    this.offer.departureDate = request.departureDate;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };


    this.http.post<Offer>(url, this.offer, httpOptions).subscribe(data => {
      this.offer = data;
      this.showNotification('Nabídka odeslána!', 'green-snackbar');
    }, error => {
    });
  }

  showNotification(message: string, calssName: string) {
    this.snackBar.open(message, 'Zavřít', {duration: 10000, panelClass: calssName});

  }

  formatLabel(value: number | null) {
    if (!value) {
      return '';
    }

    return value + '%';
  }

}

@Component({
  selector: 'receivable-dialog-customer',
  templateUrl: 'dialogs/receivable-dialog-customer.html',
  styleUrls: ['./customer-transport-requests.component.css']
})
export class ReceivableDialogCustomer {

  offer: Offer;

  constructor(
    public dialogRef: MatDialogRef<ReceivableDialogBusinessMan>, private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private snackBar: MatSnackBar) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

    let url = `/api/offers/${this.data.request.id}`;
    this.http.get<Offer>(url,).subscribe(data => {
        this.offer = data;
      },
      error => {
      })
  }


  generateOffer(offerId) {

    let url = `/api/offers/generate-offer/${offerId}`;
    this.http.get<Offer>(url).subscribe(data => {
      this.offer = data;
    }, error => {
    });
  }


  sendOffer(request: Receivable) {
    let url = `/api/offers/${request.id}`;

    this.offer.deliveryDate = request.deliveryDate;
    this.offer.departureDate = request.departureDate;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    this.http.post<Offer>(url, this.offer, httpOptions).subscribe(data => {
      this.offer = data;
      this.showNotification('Nabídka odeslána!', 'green-snackbar');
    }, error => {
      this.showNotification('Něco se pokazilo!', 'red-snackbar');
    });
  }

  acceptOffer(pathId) {
    let url = `/api/offers/deccission/${this.offer.id}`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    this.http.post<Offer>(url, {accepted: true, pathID: pathId}, httpOptions).subscribe(data => {
      this.offer = data;
      this.showNotification('Nabídka akceptována!', 'green-snackbar');
    }, error => {
      this.showNotification('Něco se pokazilo!', 'red-snackbar');
    });

  }

  showNotification(message: string, calssName: string) {
    this.snackBar.open(message, 'Zavřít', {duration: 10000, panelClass: calssName});

  }
}

@Component({
  selector: 'stations-dialog',
  templateUrl: 'dialogs/stations-dialog.html',
  styleUrls: ['./customer-transport-requests.component.css']
})
export class StationsDialog {

  offer: Offer;

  constructor(
    public dialogRef: MatDialogRef<ReceivableDialogBusinessMan>, private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

    let url = `/api/offers/${this.data.request.id}`;
    this.http.get<Offer>(url,).subscribe(data => {
        this.offer = data;
      },
      error => {
      })
  }
}


export interface DialogData {
  request: Receivable;
}
