import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CustomerTransportRequestsComponent} from './customer-transport-requests.component';

describe('CustomerTransportRequestsComponent', () => {
  let component: CustomerTransportRequestsComponent;
  let fixture: ComponentFixture<CustomerTransportRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerTransportRequestsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTransportRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
