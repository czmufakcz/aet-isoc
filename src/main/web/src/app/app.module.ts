import {BrowserModule, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LOCALE_ID, NgModule} from '@angular/core';

import {DatePipe, registerLocaleData} from '@angular/common';

import cs from '@angular/common/locales/cs';

import {AppComponent} from './app.component';
import {LeftNavigationBarComponent} from './left-navigation-bar/left-navigation-bar.component';
import {LayoutModule} from '@angular/cdk/layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  GestureConfig,
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSnackBarModule,
  MatTableModule,
  MatToolbarModule,
  MatTreeModule
} from '@angular/material';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {TokenInterceptorService} from "./user-service/token-interceptor.service";
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';

import {RouterModule, Routes} from '@angular/router';
import {ProcessesComponent} from './processes/processes.component';
import {UserService} from "./user-service/user.service";
import {LoginComponent} from './login/login.component';
import {OAuthModule} from "angular-oauth2-oidc";
import {ProcessesGuardService} from "./processes/processes-guard.service";
import {RegistrationComponent} from './registration/registration.component';
import {RegistrationGuardService} from "./registration/registration-guard.service";
import {CreateRequestComponent} from './create-request/create-request.component';
import {
  CustomerTransportRequestsComponent,
  ReceivableDialogBusinessMan,
  ReceivableDialogCustomer,
  StationsDialog
} from './customer-transport-requests/customer-transport-requests.component';
import {TrainCheckComponent} from './train-check/train-check.component';
import {StationsComponent} from './stations/stations.component';

import {FlexLayoutModule} from "@angular/flex-layout";
import {StatsComponent} from './stats/stats.component';

registerLocaleData(cs);

const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent, canActivate: [RegistrationGuardService]},
  {path: 'create-request', component: CreateRequestComponent, canActivate: [ProcessesGuardService]},
  {path: 'transport-requests', component: CustomerTransportRequestsComponent, canActivate: [ProcessesGuardService]},
  {path: 'train-check', component: TrainCheckComponent, canActivate: [ProcessesGuardService]},
  {path: 'stats', component: StatsComponent, canActivate: [ProcessesGuardService]}
];

@NgModule({
  declarations: [
    AppComponent,
    LeftNavigationBarComponent,
    ProcessesComponent,
    LoginComponent,
    RegistrationComponent,
    CreateRequestComponent,
    CustomerTransportRequestsComponent,
    ReceivableDialogCustomer,
    ReceivableDialogBusinessMan,
    StationsDialog,
    TrainCheckComponent,
    StationsComponent,
    StatsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSnackBarModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatPaginatorModule,
    MatDialogModule,
    MatTreeModule,
    MatExpansionModule,
    MatSliderModule,

    RouterModule.forRoot(appRoutes, {useHash: true}),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['http://localhost:8080/api'],
        sendAccessToken: true
      }
    })
  ],
  providers: [
    {provide: LOCALE_ID, useValue: "cs"},
    {provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig},

    UserService, ProcessesGuardService, RegistrationGuardService, DatePipe, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent],
  entryComponents: [CustomerTransportRequestsComponent,
    ReceivableDialogBusinessMan, ReceivableDialogCustomer, StationsDialog]

})
export class AppModule { }
