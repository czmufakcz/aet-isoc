import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from "@angular/material";
import {Offer} from "../create-request/create-request.component";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-train-check',
  templateUrl: './train-check.component.html',
  styleUrls: ['./train-check.component.css'],
})
export class TrainCheckComponent implements OnInit {

  displayedColumns: string[] = ['fromCity', 'toCity', 'departureDate', 'deliveryDate', 'commodityType', 'quantity', 'receivableState', 'offerState'];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public offersDatasource: MatTableDataSource<Offer> = new MatTableDataSource();

  selectedOffer: Offer;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.loadRequests();
  }


  loadRequests() {
    this.getReceivablesForCustomer().subscribe(data => {
        this.offersDatasource.data = data;
      },
      error => {
      })
  }


  getReceivablesForCustomer(): Observable<Offer[]> {
    return this.http.get<Offer[]>('/api/offers/state/ACCEPTED_BY_CUSTOMER');
  }

  selectRow(row) {
    this.selectedOffer = row;
  }
}
