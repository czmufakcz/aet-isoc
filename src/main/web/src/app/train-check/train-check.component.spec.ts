import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TrainCheckComponent} from './train-check.component';

describe('TrainCheckComponent', () => {
  let component: TrainCheckComponent;
  let fixture: ComponentFixture<TrainCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TrainCheckComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
