import {Injectable} from '@angular/core';
import {OAuthService} from "angular-oauth2-oidc";
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RegistrationGuardService implements CanActivate {

  constructor(private oauthService: OAuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    var hasAccessToken = this.oauthService.hasValidAccessToken();
    return (!hasAccessToken);
  }
}
