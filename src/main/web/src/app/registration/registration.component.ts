import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MatSnackBar} from "@angular/material";
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef, private http: HttpClient, private snackBar: MatSnackBar, private router: Router) {

  }

  public buildForm() {
    this.formGroup = this.formBuilder.group({
      firstName: [''],
      lastName: ['',],
      email: ['',],
      telephone: ['',],
      password1: ['',],
      password2: ['',],
      street: ['',],
      houseNumber: ['',],
      city: ['',],
      postalNumber: ['',],
      representingCompany: ['',],
      icNumber: ['',],
      dicNumber: ['',],
    });
  }

  ngOnInit() {
    this.buildForm();
  }

  changeRequired(val) {

    if (val) {
      this.formGroup.get('icNumber').setValidators(Validators.required);
      this.formGroup.get('icNumber').updateValueAndValidity();
      this.formGroup.get('dicNumber').setValidators(Validators.required);
      this.formGroup.get('dicNumber').updateValueAndValidity();
    }
    else {
      this.formGroup.get('icNumber').setValidators(null);
      this.formGroup.get('icNumber').updateValueAndValidity();
      this.formGroup.get('dicNumber').setValidators(null);
      this.formGroup.get('dicNumber').updateValueAndValidity();
    }
  }

  register() {
    let self = this;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    let customer: Customer = {
      firstname: this.formGroup.get('firstName').value,
      lastname: this.formGroup.get('lastName').value,
      email: this.formGroup.get('email').value,
      phone: this.formGroup.get('telephone').value,
      password: this.formGroup.get('password1').value,
      cin: this.formGroup.get('icNumber').value ? this.formGroup.get('icNumber').value : null,
      vatin: this.formGroup.get('dicNumber').value ? this.formGroup.get('dicNumber').value : null,
      company: this.formGroup.get('representingCompany').value ? this.formGroup.get('representingCompany').value : false,
      address: {
        street: this.formGroup.get('street').value,
        descriptionNumber: this.formGroup.get('houseNumber').value,
        city: this.formGroup.get('city').value,
        postcode: this.formGroup.get('postalNumber').value,
      }
    };


    this.http.post("/api/customers", customer, httpOptions).subscribe(data => {
      this.formGroup.reset();
      this.router.navigateByUrl("/");
      self.showNotification('Customer account successfully created', 'green-snackbar');
    }, error => {
      self.showNotification('Cant create customer for some reason', 'red-snackbar');
    });
  }

  showNotification(message: string, calssName: string) {
    this.snackBar.open(message, 'Close', {duration: 10000, panelClass: calssName});
  }
}

export interface Address {
  street: any;
  descriptionNumber: any;
  city: any;
  postcode: any;
}

export interface Customer {
  email: any;
  firstname: any;
  lastname: any;
  phone: any;
  password: any;
  address: Address
  cin: any;
  vatin: any;
  company: any;
}
