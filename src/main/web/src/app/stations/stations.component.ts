import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {Offer} from "../create-request/create-request.component";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.css']
})
export class StationsComponent implements OnInit {

  @Input() offer: Offer;
  @Input() isTransitier: boolean;
  nextSelectableId: number = -1;
  cities: City[] = [];
  intervalHolder: any;
  SETTINGS = {
    navBarTravelling: false,
    navBarTravelDirection: "",
    navBarTravelDistance: 150
  };
  pnAdvancerLeft;
  pnAdvancerRight;
  pnProductNav;
  pnProductNavContents;


  // getCitiesOnPath(): Observable<City[]> {
  //
  //   let url = `/api/offers/${this.offer.id}/getAllCitiesOnPath`;
  //
  //   return this.http.get<City[]>(url);
  // }
  last_known_scroll_position = 0;
  ticking = false;

  constructor(private http: HttpClient, private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnChanges(changes) {
    this.updateSelectedIndex();
  }

  selectCity(cityName) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    let url = `/api/offers/${this.offer.id}/changeCity`;

    this.http.post<Offer>(url, cityName).subscribe(data => {
        this.offer = data;

        this.changeDetectorRef.markForCheck();
        this.updateSelectedIndex();
      },
      error => {
      })

  }

  ngOnDestroy(): void {
    clearInterval(this.intervalHolder);
  }

  ngOnInit() {

    this.intervalHolder = setInterval(() => {
      this.doSomething('s');
      this.changeDetectorRef.markForCheck();
    }, 1000); // 1 minute
  }

  ngAfterViewInit() {
    this.initialize();

  }

  initialize() {
    let self = this;
    self.pnAdvancerLeft = document.getElementById("pnAdvancerLeft");
    self.pnAdvancerRight = document.getElementById("pnAdvancerRight");

    self.pnProductNav = document.getElementById("pnProductNav");
    self.pnProductNavContents = document.getElementById("pnProductNavContents");

    self.pnProductNav.setAttribute("data-overflowing", self.determineOverflow(self.pnProductNavContents, self.pnProductNav));

    this.pnProductNav.addEventListener("scroll", function () {
      self.last_known_scroll_position = window.scrollY;
      if (!self.ticking) {
        window.requestAnimationFrame(function () {
          self.doSomething(self.last_known_scroll_position);
          self.ticking = false;
        });
      }
      self.ticking = true;
    });


    this.pnAdvancerLeft.addEventListener("click", function () {
      // If in the middle of a move return
      if (self.SETTINGS.navBarTravelling === true) {
        return;
      }
      // If we have content overflowing both sides or on the left
      if (self.determineOverflow(self.pnProductNavContents, self.pnProductNav) === "left" || self.determineOverflow(self.pnProductNavContents, self.pnProductNav) === "both") {
        // Find how far this panel has been scrolled
        var availableScrollLeft = self.pnProductNav.scrollLeft;
        // If the space available is less than two lots of our desired distance, just move the whole amount
        // otherwise, move by the amount in the settings
        if (availableScrollLeft < self.SETTINGS.navBarTravelDistance * 2) {
          self.pnProductNavContents.style.transform = "translateX(" + availableScrollLeft + "px)";
        } else {
          self.pnProductNavContents.style.transform = "translateX(" + self.SETTINGS.navBarTravelDistance + "px)";
        }
        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
        self.pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
        // Update our settings
        self.SETTINGS.navBarTravelDirection = "left";
        self.SETTINGS.navBarTravelling = true;
      }
      // Now update the attribute in the DOM
      self.pnProductNav.setAttribute("data-overflowing", self.determineOverflow(self.pnProductNavContents, self.pnProductNav));
    });

    this.pnAdvancerRight.addEventListener("click", function () {
      // If in the middle of a move return
      if (self.SETTINGS.navBarTravelling === true) {
        return;
      }
      // If we have content overflowing both sides or on the right
      if (self.determineOverflow(self.pnProductNavContents, self.pnProductNav) === "right" || self.determineOverflow(self.pnProductNavContents, self.pnProductNav) === "both") {
        // Get the right edge of the container and content
        var navBarRightEdge = self.pnProductNavContents.getBoundingClientRect().right;
        var navBarScrollerRightEdge = self.pnProductNav.getBoundingClientRect().right;
        // Now we know how much space we have available to scroll
        var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
        // If the space available is less than two lots of our desired distance, just move the whole amount
        // otherwise, move by the amount in the settings
        if (availableScrollRight < self.SETTINGS.navBarTravelDistance * 2) {
          self.pnProductNavContents.style.transform = "translateX(-" + availableScrollRight + "px)";
        } else {
          self.pnProductNavContents.style.transform = "translateX(-" + self.SETTINGS.navBarTravelDistance + "px)";
        }
        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
        self.pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
        // Update our settings
        self.SETTINGS.navBarTravelDirection = "right";
        self.SETTINGS.navBarTravelling = true;
      }
      // Now update the attribute in the DOM
      self.pnProductNav.setAttribute("data-overflowing", self.determineOverflow(self.pnProductNavContents, self.pnProductNav));
    });

    this.pnProductNavContents.addEventListener(
      "transitionend",
      function () {
        // get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
        var styleOfTransform = window.getComputedStyle(self.pnProductNavContents, null);
        var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
        // If there is no transition we want to default to 0 and not null
        var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
        self.pnProductNavContents.style.transform = "none";
        self.pnProductNavContents.classList.add("pn-ProductNav_Contents-no-transition");
        // Now lets set the scroll position
        if (self.SETTINGS.navBarTravelDirection === "left") {
          self.pnProductNav.scrollLeft = self.pnProductNav.scrollLeft - amount;
        } else {
          self.pnProductNav.scrollLeft = self.pnProductNav.scrollLeft + amount;
        }
        self.SETTINGS.navBarTravelling = false;
      },
      false
    );
  }

  doSomething(scroll_pos) {
    this.pnProductNav.setAttribute("data-overflowing", this.determineOverflow(this.pnProductNavContents, this.pnProductNav));
  }

  determineOverflow(content, container) {
    var containerMetrics = container.getBoundingClientRect();
    var containerMetricsRight = Math.floor(containerMetrics.right);
    var containerMetricsLeft = Math.floor(containerMetrics.left);
    var contentMetrics = content.getBoundingClientRect();
    var contentMetricsRight = Math.floor(contentMetrics.right);
    var contentMetricsLeft = Math.floor(contentMetrics.left);

    if ((containerMetricsLeft - 1) > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {

      return "both";
    } else if (contentMetricsRight > containerMetricsRight) {
      return "right";
    } else if ((contentMetricsLeft) < (containerMetricsLeft - 1)) {
      return "left";
    } else {
      return "none";
    }
  }

  private updateSelectedIndex() {
    this.nextSelectableId = -1;

    let BreakException = {};

    let self = this;
    this.offer.trackingStation.forEach(function (city, index) {
      if (!city.changed) {
        self.nextSelectableId = city.id;
        throw BreakException;
      }
    });
  }

}

export class City {
  id: any;
  city: any;
  changed: any;
  canSelect: any
}
