import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {

  stats: Stats;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.loadStats().subscribe(data => {
      console.log(data);
      this.stats = data;
    })
  }

  loadStats() {
    return this.http.get<Stats>('api/statistics/lastMonth')
  }
}


export class Stats {
  acceptedOffer: any;
  canceledOffer: any;
  canceledReceivable: any;
  createdOffer: any;
  createdReceivable: any;
  deliveredOffer: any;
  doneReceivable: any
}
