import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MatSnackBar} from "@angular/material";
import {Router} from "@angular/router";
import {map, startWith} from "rxjs/operators";
import {User} from "../login/login.component";
import {City} from "../stations/stations.component";

@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.css']
})
export class CreateRequestComponent implements OnInit {

  public formGroup: FormGroup;

  public formControlCityFrom: FormControl = new FormControl();
  public formControlCityTo: FormControl = new FormControl();
  public availableCities;
  public filteredCitiesFrom;
  public filteredCitiesTo;
  public commodities;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private snackBar: MatSnackBar, private router: Router) {

  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      quantity: [''],
      commodity: [''],
      departureDate: [''],
      deliveryDate: [''],

    });
    this.http.get("/api/receivables/fromCities").subscribe(data => {
        this.availableCities = data;

        this.filteredCitiesFrom = this.formControlCityFrom.valueChanges
          .pipe(
            startWith(''),
            map(city => city ? this._filterCities(city) : this.availableCities.slice())
          );

        this.filteredCitiesTo = this.formControlCityTo.valueChanges
          .pipe(
            startWith(''),
            map(city => city ? this._filterCities(city) : this.availableCities.slice())
          );
      },
      error => {

      }
    );

    this.http.get("/api/receivables/commodities").subscribe(data => {
        this.commodities = data;
      },
      error => {

      }
    )

  }

  create() {
    let receivable: Receivable = {
      fromCity: this.formControlCityFrom.value,
      toCity: this.formControlCityTo.value,
      departureDate: this.formGroup.get('departureDate').value,
      deliveryDate: this.formGroup.get('deliveryDate').value,
      commodityType: this.formGroup.get('commodity').value.name,
      quantity: this.formGroup.get('quantity').value
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    this.http.post("/api/receivables", receivable, httpOptions).subscribe(data => {
      this.formGroup.reset();
      this.showNotification('Požadavek úspěšně vytvořen!', 'green-snackbar');
      this.router.navigateByUrl("/transport-requests");
    }, error => {
      this.showNotification('Něco se pokazilo!', 'red-snackbar');
    });
  }

  private _filterCities(value: string) {
    const filterValue = value.toLowerCase();
    return this.availableCities.filter(city => city.toLowerCase().indexOf(filterValue) === 0);
  }

  showNotification(message: string, calssName: string) {
    this.snackBar.open(message, 'Zavřít', {duration: 10000, panelClass: calssName});
  }
}

export interface Receivable {
  id?: any,
  fromCity: any,
  toCity: any,
  departureDate: any,
  deliveryDate: any,
  commodityType: any,
  quantity: any
  customer?: User
}

export interface Offer {
  id: any,
  original?: Receivable
  customer?: User,
  businessman?: User,
  departureDate: any,
  deliveryDate: any,
  expiredDate: any,
  finalPrice: any,
  priceResource: any,
  offerState: any,
  sale: any,
  trackingStation: City[],
  paths: Path[],
  resources: Resource[]
}

export interface Path {
  id: any,
  accepted: any,
  connections: Connection[],
  priceByConnections: any,
  toCity: any,
  fromCity: any,
  timeLength: any
}

export interface Connection {
  id: any,
  fromCity: any,
  toCity: any,
  duration: any,
  bandwidth: any
}

export interface Resource {
  id: any,
  train: any,
  wagons: Wagon[]
}

export interface Wagon {
  id: any,
  identification: any,
  loadCapacity: any,
  unitType: any,
  lease: any
}
