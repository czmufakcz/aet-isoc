import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material";


@Component({
  selector: 'app-processes',
  templateUrl: './processes.component.html',
  styleUrls: ['./processes.component.css']
})
export class ProcessesComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'started', 'price'];

  processesSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  constructor() {
  }

  ngOnInit() {
  }

}

export interface PeriodicElement {
  id: number;
  name: string;
  started: Date;
  price: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, name: "Process Name", started: new Date(), price: 1002},
  {id: 2, name: "Process Name", started: new Date(), price: 1002},
  {id: 3, name: "Process Name", started: new Date(), price: 1002},
  {id: 4, name: "Process Name", started: new Date(), price: 1002},
  {id: 5, name: "Process Name", started: new Date(), price: 1002},
  {id: 6, name: "Process Name", started: new Date(), price: 1002},
  {id: 7, name: "Process Name", started: new Date(), price: 1002},
  {id: 8, name: "Process Name", started: new Date(), price: 1002},
  {id: 9, name: "Process Name", started: new Date(), price: 1002},
  {id: 10, name: "Process Name", started: new Date(), price: 1002},
  {id: 11, name: "Process Name", started: new Date(), price: 1002},
  {id: 12, name: "Process Name", started: new Date(), price: 1002},
  {id: 13, name: "Process Name", started: new Date(), price: 1002},
  {id: 14, name: "Process Name", started: new Date(), price: 1002},
  {id: 15, name: "Process Name", started: new Date(), price: 1002},
  {id: 16, name: "Process Name", started: new Date(), price: 1002},
  {id: 17, name: "Process Name", started: new Date(), price: 1002},
  {id: 18, name: "Process Name", started: new Date(), price: 1002},
  {id: 19, name: "Process Name", started: new Date(), price: 1002},
  {id: 20, name: "Process Name", started: new Date(), price: 1002}
];
