import {Component, OnInit} from '@angular/core';
import {Roles, UserService} from "../user-service/user.service";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials = {username: '', password: ''};

  user: User = {firstname: '', lastname: '', email: '', role: ''};

  constructor(private app: UserService, private http: HttpClient, private router: Router, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    if (this.isUserLoggedIn()) {
      this.getUserInfo(this, false);
    }
  }

  login() {
    let self = this;
    this.app.obtainAccessToken(this.credentials).then(data => {
      this.getUserInfo(self, true);

    }, function () {
      self.showNotification('Něco se pokazilo během přihlášení!', 'red-snackbar');
      self.clearInputs();
    });
  }

  showNotification(message: string, calssName: string) {
    this.snackBar.open(message, 'Zavřít', {duration: 10000, panelClass: calssName});

  }

  logout() {
    this.app.logout();
    this.clearInputs();
    this.router.navigateByUrl("/");
    location.reload();
    this.showNotification('Uživatel byl úspěšně odhlášen!', 'green-snackbar');
  }

  isUserLoggedIn() {
    return this.app.isLoggedIn();
  }

  getLabelForRole(role) {
    if (Roles.BUSINESSMAN === role) {
      return "Obchodník"
    }

    if (Roles.CUSTOMER === role) {
      return "Zákazník"
    }

    if (Roles.TRANSITER === role) {
      return "Tranzitér"
    }

    if (Roles.ADMIN === role) {
      return "Admin"
    }

    return "";
  }

  register() {
    this.router.navigateByUrl("/registration");
  }

  clearInputs() {
    this.credentials = {username: '', password: ''}
  }

  private getUserInfo(self, info) {
    self.http.get("/api/users/whoLogged").subscribe(data => {
      self.user = data;
      if (info) {
        self.showNotification('Uživatel byl úspěšně přihlášen!', 'green-snackbar');
        if (this.user.role === Roles.TRANSITER) {
          this.router.navigateByUrl("/train-check");
        }
        if (this.user.role === Roles.CUSTOMER || this.user.role === Roles.BUSINESSMAN) {
          this.router.navigateByUrl("/transport-requests");
        }
      }
    }, function () {
      if (info) {
        self.showNotification('Nesprávný email nebo heslo!', 'red-snackbar');
      }
    }, function () {
      self.clearInputs();
    });
  }
}

export interface User {
  firstname: any,
  lastname: any,
  email: any,
  role: any
}
