import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {OAuthService} from "angular-oauth2-oidc";
import {User} from "../login/login.component";

@Injectable({
  providedIn: 'root'

})
export class UserService {

  private user: User;

  constructor(
    private _router: Router, private _http: HttpClient, private oauthService: OAuthService) {

    this.oauthService.loginUrl = window.location.origin + '/processes';
    this.oauthService.tokenEndpoint = "http://localhost:8080/oauth/token";
    this.oauthService.clientId = "aet";
    this.oauthService.scope = "read write";
    this.oauthService.setStorage(sessionStorage);
    this.oauthService.dummyClientSecret = "isoc";
    this.oauthService.useHttpBasicAuthForPasswordFlow = true;
    this.oauthService.oidc = false;
    this.oauthService.logoutUrl = 'http://localhost:8080';

  }

  obtainAccessToken(cretendtials): Promise<Object> {
    return this.oauthService.fetchTokenUsingPasswordFlow(cretendtials.username, cretendtials.password);
  }


  isLoggedIn() {
    return this.oauthService.getAccessToken() !== null;
  }


  logout() {
    this.oauthService.logOut();
  }

  async getUserInfo() {
    let self = this;

    if (this.isLoggedIn()) {
      if (this.user) {
        return this.user;
      }
      else {
        this.user = await this.callGetUserInfo();
        return this.user;
      }
    }

    return null;
  }

  async callGetUserInfo(): Promise<User> {
    return await this._http.get<User>('/api/users/whoLogged').toPromise();
  }

}

export enum Roles {
  CUSTOMER = 'CUSTOMER',
  ADMIN = 'ADMIN',
  BUSINESSMAN = 'BUSINESSMAN',
  TRANSITER = 'TRANSITER',
  UNDEFINED = 'UNDEFINED'
}
