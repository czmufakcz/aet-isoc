import {Injectable} from '@angular/core';
import {Observable} from "rxjs/internal/Observable";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {OAuthService} from "angular-oauth2-oidc";

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private userService: OAuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.userService.getAccessToken() !== null) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${this.userService.getAccessToken()}`
        }
      });
    }

    return next.handle(req);
  }
}
