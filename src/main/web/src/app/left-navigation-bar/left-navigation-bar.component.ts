import {Component} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Roles, UserService} from "../user-service/user.service";
import {User} from "../login/login.component";
import {HttpClient} from "@angular/common/http";
import {Offer} from "../create-request/create-request.component";

@Component({
  selector: 'app-left-navigation-bar',
  templateUrl: './left-navigation-bar.component.html',
  styleUrls: ['./left-navigation-bar.component.css']
})
export class LeftNavigationBarComponent {

  roles: typeof Roles = Roles;
  private user: User;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private app: UserService, private http: HttpClient) {

  }

  getReceivablesForCustomer(): Observable<Offer[]> {
    return this.http.get<Offer[]>('/api/offers/state/ACCEPTED_BY_CUSTOMER');
  }


  getUserRole() {
    if (this.app.isLoggedIn()) {
      if (this.user) {
        return this.user;
      }
      else {
        this.http.get<User>('/api/users/whoLogged').toPromise().then(data => {
          this.user = data;
          return this.user;
        })

      }
    } else {
      this.user = null;
      return this.user;
    }
  }

}
