import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {MatSidenavModule} from '@angular/material/sidenav';
import {LeftNavigationBarComponent} from './left-navigation-bar.component';

describe('LeftNavigationBarComponent', () => {
  let component: LeftNavigationBarComponent;
  let fixture: ComponentFixture<LeftNavigationBarComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatSidenavModule],
      declarations: [LeftNavigationBarComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(LeftNavigationBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
